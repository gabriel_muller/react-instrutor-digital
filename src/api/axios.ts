import axios from "axios";
import { envConfig } from "../../env-config";
import { toast } from "@/components/ui/use-toast";

const axiosInstance = axios.create({
  baseURL: envConfig.baseUrl,
});

const isTokenExpired = () => {
  const expiresIn = localStorage.getItem("expires_in");
  const access_token = localStorage.getItem("access_token");
  if (!expiresIn || !access_token) {
    return true;
  }
  const expiryTime = new Date(parseInt(expiresIn, 10));
  return expiryTime < new Date();
};

const showToast = () => {
  toast({
    title: "Você foi deslogado!",
    variant: "destructive",
    className: "bg-red-700 border-none antialiasing",
    description: "Estamos te redirecionado para a tela de login.",
    duration: 2000,
  });
};

const handleTokenExpiration = () => {
  if (isTokenExpired()) {
    localStorage.removeItem("access_token");
    localStorage.removeItem("expires_in");
    showToast();
    setTimeout(() => {
      window.location.href = "/login";
    }, 2000);
  }
};

setInterval(handleTokenExpiration, 36000000);

axiosInstance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("access_token");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const { response } = error;
    if (response && response.status === 401) {
      handleTokenExpiration();
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;
