import { Sidebar } from "./components/ui/sidebar";
import { ToggleTheme } from "./components/ui/toggle-theme";
import { Button } from "./components/ui/button";
import { Menu } from "lucide-react";
import { SidebarProvider, useSidebar } from "./contexts/sidebar-context";

const Layout = ({ children }: any) => {
  return (
    <SidebarProvider>
      <LayoutContent>{children}</LayoutContent>
    </SidebarProvider>
  );
};

const LayoutContent = ({ children }: any) => {
  const { collapsed, toggleCollapsed } = useSidebar();

  return (
    <div className="flex h-screen">
      <Sidebar collapsed={collapsed} />
      <div className="flex flex-col flex-1">
        <div className="flex items-center justify-between my-8 mx-4">
          <Button variant="outline" size="icon" onClick={toggleCollapsed}>
            <Menu className="h-[1.2rem] w-[1.2rem] rotate-0 scale-100 transition-all dark:-rotate-90 dark:scale-0" />
            <Menu className="absolute h-[1.2rem] w-[1.2rem] rotate-90 scale-0 transition-all dark:rotate-0 dark:scale-100" />
            <span className="sr-only" />
          </Button>
          <ToggleTheme />
        </div>
        <main className="p-6 flex-1 overflow-y-auto">{children}</main>
      </div>
    </div>
  );
};

export default Layout;
