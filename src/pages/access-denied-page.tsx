import { Button } from "@/components/ui/button";
import { CircleArrowLeft } from "lucide-react";
import { useNavigate } from "react-router-dom";

export function AccessDeniedPage() {
  const navigate = useNavigate();

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-gray-900 p-4">
      <img
        src="/access_denied.svg"
        alt="Acesso negado"
        className="mb-8 w-1/3 max-h-60"
      />
      <p className="text-3xl font-bold text-gray-100">
        Oops... Você não tem acesso a essa página
      </p>
      <p className="text-lg text-gray-400 mt-4">
        Solicite a permissão para o seu gestor.
      </p>
      <Button
        className="text-white bg-[#F55139]/70 hover:bg-[#F55139]/60 my-8 flex items-center"
        onClick={() => navigate(-2)}
      >
        <CircleArrowLeft className="w-5 h-5 mr-2 mb-[2px]" />
        <span className="text-lg">Voltar</span>
      </Button>
    </div>
  );
}
