import {
  Form,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
} from "@/components/ui/form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { Input } from "@/components/ui/input";
import { LoadingButton } from "@/components/ui/loading-button";
import { Eye, EyeOff, KeyRound } from "lucide-react";
import { useState } from "react";
import { useToast } from "@/components/ui/use-toast";

const resetPasswordSchema = z
  .object({
    password: z.string({ required_error: "Campo é obrigatório" }),
    newPassword: z.string({ required_error: "Campo é obrigatório" }),
    newPasswordConfirmation: z.string({
      required_error: "Campo é obrigatório",
    }),
  })
  .refine((data) => data.newPassword === data.newPasswordConfirmation, {
    path: ["newPasswordConfirmation"],
    message: "As senhas precisam ser iguais",
  });

type ResetPasswordProps = {
  password: string;
  newPassword: string;
  newPasswordConfirmation: string;
};

type ResetPasswordSchema = z.infer<typeof resetPasswordSchema>;

export function ResetPasswordPage() {
  const form = useForm<ResetPasswordSchema>({
    resolver: zodResolver(resetPasswordSchema),
  });

  const { toast } = useToast();

  const [showPassword, setShowPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [
    showNewPasswordConfirmation,
    setShowNewPasswordConfirmation,
  ] = useState(false);
  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const toggleNewPasswordVisibility = () => {
    setShowNewPassword(!showNewPassword);
  };

  const toggleNewPasswordConfirmationVisibility = () => {
    setShowNewPasswordConfirmation(!showNewPasswordConfirmation);
  };

  const handleResetPassword = async (_data: ResetPasswordProps) => {
    toast({
      title: "Funcionalidade ainda em desenvolvimento!",
      description: `Aguarde os próximos pacotes para obter esta funcionalidade`,
      className: "bg-sky-700 border-none antialiasing",
      duration: 3000,
    });
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-slate-600 p-4">
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleResetPassword)}
          className="space-y-10 p-10 shadow-md rounded-md w-full max-w-xl bg-slate-950 flex flex-col items-center"
        >
          <img src="/logo-instrutor-digital.svg" className="self-center my-8" />
          <FormDescription className="text-xl font-semibold text-white text-center">
            Alteração de senha
          </FormDescription>
          <FormDescription className="text-sm font-medium text-white text-center">
            Se você deseja alterar a sua senha, basta realizar a troca abaixo.
          </FormDescription>
          <div className="w-full relative">
            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormItem className="flex flex-col w-full mt-5">
                  <FormLabel>
                    Senha Atual<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <div className="relative w-full">
                      <Input
                        placeholder="Digite a senha atual"
                        type={showPassword ? "text" : "password"}
                        {...field}
                        className="w-full pr-10"
                      />
                      <div
                        className="absolute inset-y-0 right-0 pr-3 flex items-center cursor-pointer"
                        onClick={togglePasswordVisibility}
                      >
                        {showPassword ? (
                          <EyeOff className="text-gray-400 w-4 h-4" />
                        ) : (
                          <Eye className="text-gray-400 w-4 h-4" />
                        )}
                      </div>
                    </div>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="w-full relative">
            <FormField
              control={form.control}
              name="newPassword"
              render={({ field }) => (
                <FormItem className="flex flex-col w-full">
                  <FormLabel>
                    Nova Senha<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <div className="relative w-full">
                      <Input
                        placeholder="Digite a nova senha"
                        type={showNewPassword ? "text" : "password"}
                        {...field}
                        className="w-full pr-10"
                      />
                      <div
                        className="absolute inset-y-0 right-0 pr-3 flex items-center cursor-pointer"
                        onClick={toggleNewPasswordVisibility}
                      >
                        {showNewPassword ? (
                          <EyeOff className="text-gray-400 w-4 h-4" />
                        ) : (
                          <Eye className="text-gray-400 w-4 h-4" />
                        )}
                      </div>
                    </div>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="w-full relative">
            <FormField
              control={form.control}
              name="newPasswordConfirmation"
              render={({ field }) => (
                <FormItem className="flex flex-col w-full">
                  <FormLabel>
                    Confirmar Nova Senha<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <div className="relative w-full">
                      <Input
                        placeholder="Digite novamente a nova senha"
                        type={showNewPasswordConfirmation ? "text" : "password"}
                        {...field}
                        className="w-full pr-10"
                      />
                      <div
                        className="absolute inset-y-0 right-0 pr-3 flex items-center cursor-pointer"
                        onClick={toggleNewPasswordConfirmationVisibility}
                      >
                        {showNewPasswordConfirmation ? (
                          <EyeOff className="text-gray-400 w-4 h-4" />
                        ) : (
                          <Eye className="text-gray-400 w-4 h-4" />
                        )}
                      </div>
                    </div>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="flex justify-center w-full mt-3">
            <LoadingButton
              type="submit"
              loading={false}
              className="w-full text-white bg-blue-700 hover:bg-blue-800 mb-4"
            >
              <KeyRound className="w-4 h-4 mr-2" />
              Alterar Senha
            </LoadingButton>
          </div>
        </form>
      </Form>
    </div>
  );
}
