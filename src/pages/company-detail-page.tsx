import { useQuery } from "@tanstack/react-query";
import { useNavigate, useParams } from "react-router-dom";
import {
  ArrowLeft,
  PlusCircle,
  Download,
  Eye,
  Pencil,
  Trash2,
  UserCog,
  Truck,
  Wrench,
} from "lucide-react";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Separator } from "@/components/ui/separator";
import { formatCNPJ } from "@/shared/functions/format-cnpj";
import { formatCEP } from "@/shared/functions/format-cep";
import { formatPhoneNumber } from "@/shared/functions/format-phone-number";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { formatCPF } from "@/shared/functions/format-cpf";
import { Dialog, DialogTrigger } from "@/components/ui/dialog";
import { AlertDialog, AlertDialogTrigger } from "@radix-ui/react-alert-dialog";
import { UF } from "@/shared/enums/state-enum";
import { GlobalAccessLevel } from "@/shared/enums/access-level-enum";
import { Skeleton } from "@/components/ui/skeleton";
import { Spinner } from "@/components/ui/spinner";
import axiosInstance from "@/api/axios";
import {
  CreateBranchDialog,
  DeleteBranchDialog,
  DetailBranchDialog,
  UpdateBranchDialog,
} from "@/components/company/branch";
import {
  CreateVehicleDialog,
  DeleteVehicleDialog,
  DetailVehicleDialog,
  UpdateVehicleDialog,
  VinculateDriverToVehicleDialog,
  VinculateEquipmentToVehicleDialog,
} from "@/components/company/vehicle";
import {
  CreateDriverDialog,
  DeleteDriverDialog,
  DetailDriverDialog,
  UpdateDriverDialog,
  VinculateVehicleToDriverDialog,
} from "@/components/company/driver";
import {
  CreateUserDialog,
  DeleteUserDialog,
  DetailUserDialog,
  UpdateUserDialog,
} from "@/components/company/user";
import { usePermissions } from "@/contexts/auth-context";
import { parseAccessLevel } from "@/shared/functions";

type FindCompanyByIdProps = {
  cnpj: string;
  socialReason: string;
  fantasyName: string;
  address: string;
  addressNumber: string;
  addressComp: string;
  city: {
    name: string;
    state: UF;
  };
  addressUf: string;
  zipcode: string;
  email: string;
  emailSecondary: string;
  phone: string;
  phoneSecondary: string;
  branches: Branch[];
  companyDriver: Driver[];
  companyVehicle: Vehicle[];
  user: User[];
  headOffice: boolean;
};

interface Branch {
  id: string;
  cnpj: string;
  socialReason: string;
  address: string;
  addressNumber: string;
  city: {
    name: string;
    state: UF;
  };
}

interface Vehicle {
  id: string;
  licensePlate: string;
  chassi: string;
  capacity: string;
  ownerName: string;
  manufactureYear: string;
}

interface Driver {
  id: string;
  cnhNumber: string;
  cpf: string;
  name: string;
  phone: string;
}

interface User {
  id: string;
  username: string;
  name: string;
  email: string;
  accessLevel: GlobalAccessLevel;
  phone: string;
}

export function CompanyDetailPage() {
  const navigate = useNavigate();
  const { companyId } = useParams();
  const { roles } = usePermissions();

  const hasCreateBranchPermission = [
    "global_admin",
    "company_admin",
    "branch_create",
  ].some((role) => roles.includes(role));

  const hasViewBranchPermission = [
    "global_admin",
    "company_global",
    "company_view",
  ].some((role) => roles.includes(role));

  const hasEditBranchPermission = [
    "global_admin",
    "company_admin",
    "company_edit",
  ].some((role) => roles.includes(role));

  const hasRemoveBranchPermission = [
    "global_admin",
    "company_admin",
    "company_delete",
  ].some((role) => roles.includes(role));

  const hasCreateVehiclePermission = [
    "global_admin",
    "company_admin",
    "vehicle_create",
  ].some((role) => roles.includes(role));

  const hasViewVehiclePermission = [
    "global_admin",
    "company_global",
    "vehicle_view",
  ].some((role) => roles.includes(role));

  const hasEditVehiclePermission = [
    "global_admin",
    "company_admin",
    "vehicle_edit",
  ].some((role) => roles.includes(role));

  const hasRemoveVehiclePermission = [
    "global_admin",
    "company_admin",
    "vehicle_delete",
  ].some((role) => roles.includes(role));

  const hasCreateDriverPermission = [
    "global_admin",
    "company_admin",
    "driver_create",
  ].some((role) => roles.includes(role));

  const hasViewDriverPermission = [
    "global_admin",
    "company_global",
    "driver_view",
  ].some((role) => roles.includes(role));

  const hasEditDriverPermission = [
    "global_admin",
    "company_admin",
    "driver_edit",
  ].some((role) => roles.includes(role));

  const hasRemoveDriverPermission = [
    "global_admin",
    "company_admin",
    "driver_delete",
  ].some((role) => roles.includes(role));

  const findCompanyById = async (id: string) => {
    const response = await new Promise((resolve) => {
      setTimeout(async () => {
        const res = await axiosInstance.get<FindCompanyByIdProps>(
          `/company/${id}`
        );
        resolve(res);
      }, 1000);
    });
    return (response as any).data;
  };

  const { data: company, isLoading } = useQuery({
    queryKey: ["find-company-by-id", companyId],
    queryFn: () => findCompanyById(companyId as string),
  });

  return (
    <>
      <div className="flex items-center justify-start gap-4">
        <Button
          type="button"
          onClick={() => navigate(`/empresa`)}
          className="text-white antialiased bg-slate-700 hover:bg-slate-700/75"
        >
          <ArrowLeft className="w-5 h-5" />
        </Button>
        <h1 className="text-2xl text-white font-bold antialiased tracking-wide">
          {company?.socialReason}
        </h1>
      </div>
      <div className="grid grid-cols-12 items-center text-left gap-4 mb-4">
        <h1 className="col-span-12 flex flex-col text-xl text-white mt-8 font-medium">
          Dados da empresa
        </h1>
        <Separator className="col-span-12" />
        <div className="col-span-4 flex flex-col">
          <Label htmlFor="cnpj" className="my-2  text-base">
            CNPJ
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input
              id="cnpj"
              value={formatCNPJ(company ? company?.cnpj : "")}
              disabled
            />
          )}
        </div>
        <div className="col-span-4 flex flex-col">
          <Label htmlFor="socialReason" className="my-2  text-base">
            Razão Social
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input id="socialReason" value={company?.socialReason} disabled />
          )}
        </div>
        <div className="col-span-4 flex flex-col">
          <Label htmlFor="fantasyName" className="my-2  text-base">
            Nome Fantasia
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input id="fantasyName" value={company?.fantasyName} disabled />
          )}
        </div>
      </div>
      <div className="grid grid-cols-12 items-center text-left gap-4 mb-5">
        <h1 className="col-span-12 flex flex-col text-xl text-white mt-3 font-medium">
          Localização
        </h1>
        <Separator className="col-span-12" />
        <div className="col-span-3 flex flex-col">
          <Label htmlFor="zipcode" className="my-2  text-base">
            CEP
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input
              id="zipcode"
              value={formatCEP(company ? company?.zipcode : "")}
              disabled
            />
          )}
        </div>
        <div className="col-span-4 flex flex-col">
          <Label htmlFor="address" className="my-2  text-base">
            Logradouro
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input id="address" value={company?.address} disabled />
          )}
        </div>
        <div className="col-span-2 flex flex-col">
          <Label htmlFor="addressNumber" className="my-2  text-base">
            Número
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input id="addressNumber" value={company?.addressNumber} disabled />
          )}
        </div>
        <div className="col-span-3 flex flex-col">
          <Label htmlFor="addressComp" className="my-2  text-base">
            Complemento
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input
              id="addressComp"
              value={company?.addressComp ? company.addressComp : ""}
              disabled
            />
          )}
        </div>
        <div className="col-span-3 flex flex-col">
          <Label htmlFor="addressCity" className="my-2  text-base">
            Cidade
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input id="addressCity" value={company?.city.name} disabled />
          )}
        </div>
        <div className="col-span-2 flex flex-col">
          <Label htmlFor="addressUf" className="my-2 text-base">
            Estado
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input id="addressUf" value={company?.city.state} disabled />
          )}
        </div>
      </div>
      <div className="grid grid-cols-12 items-center text-left gap-4">
        <h1 className="col-span-12 flex flex-col text-xl text-white mt-3 font-medium">
          Contato
        </h1>
        <Separator className="col-span-12" />
        <div className="col-span-3 flex flex-col">
          <Label htmlFor="email" className="my-2  text-base">
            E-mail Principal
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input id="email" value={company?.email} disabled />
          )}
        </div>
        <div className="col-span-3 flex flex-col">
          <Label htmlFor="emailSecondary" className="my-2  text-base">
            E-mail Secundário
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input
              id="emailSecondary"
              value={company?.emailSecondary ? company?.emailSecondary : ""}
              disabled
            />
          )}
        </div>
        <div className="col-span-3 flex flex-col">
          <Label htmlFor="phone" className="my-2  text-base">
            Telefone Principal
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input
              id="phone"
              value={company?.phone ? formatPhoneNumber(company.phone) : ""}
              disabled
            />
          )}
        </div>
        <div className="col-span-3 flex flex-col">
          <Label htmlFor="phoneSecondary" className="my-2  text-base">
            Telefone Secundário
          </Label>
          {isLoading ? (
            <Skeleton className="w-auto h-[35px]" />
          ) : (
            <Input
              id="phoneSecondary"
              value={formatPhoneNumber(
                company?.phoneSecondary
                  ? formatPhoneNumber(company?.phoneSecondary)
                  : ""
              )}
              disabled
            />
          )}
        </div>
      </div>
      <Tabs
        defaultValue={company?.headOffice ? "branches" : "vehicles"}
        className="w-full mt-12"
      >
        <TabsList className="grid w-full grid-cols-4">
          {company?.headOffice && (
            <TabsTrigger value="branches">Filiais</TabsTrigger>
          )}
          <TabsTrigger value="vehicles">Veículos</TabsTrigger>
          <TabsTrigger value="drivers">Motoristas</TabsTrigger>
          <TabsTrigger value="users">Usuários</TabsTrigger>
        </TabsList>
        <TabsContent value="branches">
          <div className="flex items-center justify-between mt-8">
            <h1 className="text-xl text-white font-medium">Filiais</h1>
            <Dialog>
              <DialogTrigger asChild>
                <Button
                  className={`text-white ${
                    hasCreateBranchPermission
                      ? "bg-green-700 hover:bg-green-700/75"
                      : "bg-gray-400 cursor-not-allowed"
                  } disabled:cursor-not-allowed`}
                  disabled={!hasCreateBranchPermission}
                >
                  <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
                  <h1 className="text-base">Nova filial</h1>
                </Button>
              </DialogTrigger>
              <CreateBranchDialog companyId={companyId} />
            </Dialog>
          </div>
          <div className="flex items-center justify-between">
            <Input
              placeholder="Digite sua busca aqui ..."
              className="w-4/12 my-4"
            />
            <Button className="text-white bg-sky-700 hover:bg-sky-700/75">
              <Download className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base">Baixar CSV</h1>
            </Button>
          </div>
          <Table>
            <TableHeader>
              <TableHead className="text-base font-bold w-1/12">ID</TableHead>
              <TableHead className="text-base font-bold w-1/6">CNPJ</TableHead>
              <TableHead className="text-base font-bold w-1/4">
                Razão Social
              </TableHead>
              <TableHead className="text-base font-bold w-1/3">
                Endereço
              </TableHead>
              <TableHead className="text-base font-bold w-1/12">
                Ações
              </TableHead>
            </TableHeader>
            <TableBody>
              {company &&
                company.branches &&
                company.branches.map((branch: Branch) => (
                  <TableRow key={branch.id}>
                    <TableCell className="text-base">{branch.id}</TableCell>
                    <TableCell className="text-base">
                      {formatCNPJ(branch.cnpj)}
                    </TableCell>
                    <TableCell className="text-base">
                      {branch.socialReason}
                    </TableCell>
                    <TableCell className="text-base">
                      {branch.address +
                        ", " +
                        branch.addressNumber +
                        ", " +
                        branch.city.name +
                        " - " +
                        branch.city.state}
                    </TableCell>
                    <TableCell className="text-base">
                      <div className="flex items-center gap-1">
                        <Dialog>
                          <DialogTrigger asChild>
                            <button
                              title="Visualizar"
                              disabled={!hasViewBranchPermission}
                              className="disabled:cursor-not-allowed"
                            >
                              <Eye
                                className={`w-7 h-7 p-1 border rounded-md text-white ${
                                  hasViewBranchPermission
                                    ? "bg-teal-600 hover:bg-teal-600/75"
                                    : "bg-gray-400 cursor-not-allowed"
                                }`}
                              />
                            </button>
                          </DialogTrigger>
                          <DetailBranchDialog branchId={branch.id} />
                        </Dialog>

                        <Dialog>
                          <DialogTrigger asChild>
                            <button
                              title="Editar"
                              disabled={!hasEditBranchPermission}
                              className="disabled:cursor-not-allowed"
                            >
                              <Pencil
                                className={`w-7 h-7 p-1 border rounded-md text-white ${
                                  hasEditBranchPermission
                                    ? "bg-amber-600 hover:bg-amber-600/75"
                                    : "bg-gray-400 cursor-not-allowed"
                                }`}
                              />
                            </button>
                          </DialogTrigger>
                          <UpdateBranchDialog branchId={branch.id} />
                        </Dialog>
                        <AlertDialog>
                          <AlertDialogTrigger asChild>
                            <button
                              title="Remover"
                              disabled={!hasRemoveBranchPermission}
                              className="disabled:cursor-not-allowed"
                            >
                              <Trash2
                                className={`w-7 h-7 p-1 border rounded-md text-white ${
                                  hasRemoveBranchPermission
                                    ? "bg-red-600 hover:bg-red-600/75"
                                    : "bg-gray-400 cursor-not-allowed"
                                }`}
                              />
                            </button>
                          </AlertDialogTrigger>
                          <DeleteBranchDialog branchId={branch.id} />
                        </AlertDialog>
                      </div>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TabsContent>
        <TabsContent value="vehicles">
          {isLoading ? (
            <div className="flex justify-center items-center py-10">
              <Spinner size={"large"} />
            </div>
          ) : (
            <div>
              <div className="flex items-center justify-between mt-8">
                <h1 className="text-xl text-white font-medium">Veículos</h1>
                <Dialog>
                  <DialogTrigger asChild>
                    <Button
                      className={`text-white ${
                        hasCreateVehiclePermission
                          ? "bg-green-700 hover:bg-green-700/75"
                          : "bg-gray-400 cursor-not-allowed"
                      } disabled:cursor-not-allowed`}
                      disabled={!hasCreateVehiclePermission}
                    >
                      <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
                      <h1 className="text-base">Novo veículo</h1>
                    </Button>
                  </DialogTrigger>
                  <CreateVehicleDialog companyId={companyId} />
                </Dialog>
              </div>
              <div className="flex items-center justify-between">
                <Input
                  placeholder="Digite sua busca aqui ..."
                  className="w-4/12 my-4"
                />
                <Button className="text-white bg-sky-700 hover:bg-sky-700/75">
                  <Download className="w-4 h-4 mr-2 mb-[2px]" />
                  <h1 className="text-base">Baixar CSV</h1>
                </Button>
              </div>
              <Table>
                <TableHeader>
                  <TableHead className="text-base font-bold w-1/12">
                    Placa
                  </TableHead>
                  <TableHead className="text-base font-bold w-1/6">
                    Chassi
                  </TableHead>
                  <TableHead className="text-base font-bold w-1/6">
                    Capacidade
                  </TableHead>
                  <TableHead className="text-base font-bold w-1/6">
                    Motorista
                  </TableHead>
                  <TableHead className="text-base font-bold w-1/6">
                    Ano de Fabricação
                  </TableHead>
                  <TableHead className="text-base font-bold w-1/12">
                    Ações
                  </TableHead>
                </TableHeader>
                <TableBody>
                  {company &&
                    company.companyVehicle &&
                    company.companyVehicle.map((vehicle: Vehicle) => (
                      <TableRow key={vehicle.id}>
                        <TableCell className="text-base">
                          {vehicle.licensePlate}
                        </TableCell>
                        <TableCell className="text-base">
                          {vehicle.chassi}
                        </TableCell>
                        <TableCell className="text-base">
                          {vehicle.capacity}
                        </TableCell>
                        <TableCell className="text-base">
                          {vehicle.ownerName}
                        </TableCell>
                        <TableCell className="text-base">
                          {vehicle.manufactureYear}
                        </TableCell>
                        <TableCell className="text-base">
                          <div className="flex items-center gap-1">
                            <Dialog>
                              <DialogTrigger asChild>
                                <button
                                  title="Visualizar"
                                  disabled={!hasViewVehiclePermission}
                                  className="disabled:cursor-not-allowed"
                                >
                                  <Eye
                                    className={`w-7 h-7 p-1 border rounded-md text-white ${
                                      hasViewVehiclePermission
                                        ? "bg-teal-600 hover:bg-teal-600/75"
                                        : "bg-gray-400 cursor-not-allowed"
                                    }`}
                                  />
                                </button>
                              </DialogTrigger>
                              <DetailVehicleDialog
                                companyId={companyId}
                                vehicleId={vehicle.id}
                              />
                            </Dialog>

                            <Dialog>
                              <DialogTrigger asChild>
                                <button
                                  title="Editar"
                                  disabled={!hasEditVehiclePermission}
                                  className="disabled:cursor-not-allowed"
                                >
                                  <Pencil
                                    className={`w-7 h-7 p-1 border rounded-md text-white ${
                                      hasEditVehiclePermission
                                        ? "bg-amber-600 hover:bg-amber-600/75"
                                        : "bg-gray-400 cursor-not-allowed"
                                    }`}
                                  />
                                </button>
                              </DialogTrigger>
                              <UpdateVehicleDialog
                                companyId={companyId}
                                vehicleId={vehicle.id}
                              />
                            </Dialog>

                            <Dialog>
                              <DialogTrigger asChild>
                                <UserCog className="w-7 h-7 p-1 border rounded-md text-white bg-purple-600 hover:bg-purple-600/75 cursor-pointer" />
                              </DialogTrigger>
                              <VinculateDriverToVehicleDialog />
                            </Dialog>

                            <Dialog>
                              <DialogTrigger asChild>
                                <Wrench className="w-7 h-7 p-1 border rounded-md text-white bg-pink-600 hover:bg-pink-600/75 cursor-pointer" />
                              </DialogTrigger>
                              <VinculateEquipmentToVehicleDialog />
                            </Dialog>

                            <AlertDialog>
                              <AlertDialogTrigger asChild>
                                <button
                                  title="Remover"
                                  disabled={!hasRemoveVehiclePermission}
                                  className="disabled:cursor-not-allowed"
                                >
                                  <Trash2
                                    className={`w-7 h-7 p-1 border rounded-md text-white ${
                                      hasRemoveVehiclePermission
                                        ? "bg-red-600 hover:bg-red-600/75"
                                        : "bg-gray-400 cursor-not-allowed"
                                    }`}
                                  />
                                </button>
                              </AlertDialogTrigger>
                              <DeleteVehicleDialog
                                companyId={companyId}
                                vehicleId={vehicle.id}
                              />
                            </AlertDialog>
                          </div>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </div>
          )}
        </TabsContent>
        <TabsContent value="drivers">
          <div>
            <div className="flex items-center justify-between mt-8">
              <h1 className="text-xl text-white font-medium">Motoristas</h1>
              <Dialog>
                <DialogTrigger asChild>
                  <Button
                    className={`text-white ${
                      hasCreateDriverPermission
                        ? "bg-green-700 hover:bg-green-700/75"
                        : "bg-gray-400 cursor-not-allowed"
                    } disabled:cursor-not-allowed`}
                    disabled={!hasCreateDriverPermission}
                  >
                    <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
                    <h1 className="text-base">Novo motorista</h1>
                  </Button>
                </DialogTrigger>
                <CreateDriverDialog />
              </Dialog>
            </div>
            <div className="flex items-center justify-between">
              <Input
                placeholder="Digite sua busca aqui ..."
                className="w-4/12 my-4"
              />
              <Button className="text-white bg-sky-700 hover:bg-sky-700/75">
                <Download className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base">Baixar CSV</h1>
              </Button>
            </div>
            <Table>
              <TableHeader>
                <TableHead className="text-base font-bold w-1/6">CPF</TableHead>
                <TableHead className="text-base font-bold w-1/6">
                  Nome
                </TableHead>
                <TableHead className="text-base font-bold w-1/6">CNH</TableHead>
                <TableHead className="text-base font-bold w-1/6">
                  Telefone
                </TableHead>
                <TableHead className="text-base font-bold w-1/12">
                  Ações
                </TableHead>
              </TableHeader>
              <TableBody>
                {company &&
                  company.companyDriver &&
                  company.companyDriver.map((driver: Driver) => (
                    <TableRow key={driver.id}>
                      <TableCell className="text-base">
                        {formatCPF(driver.cpf)}
                      </TableCell>
                      <TableCell className="text-base">{driver.name}</TableCell>
                      <TableCell className="text-base">
                        {driver.cnhNumber}
                      </TableCell>
                      <TableCell className="text-base">
                        {formatPhoneNumber(driver.phone)}
                      </TableCell>
                      <TableCell className="text-base">
                        <div className="flex items-center gap-1">
                          <Dialog>
                            <DialogTrigger asChild>
                              <button
                                title="Visualizar"
                                disabled={!hasViewDriverPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Eye
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasViewDriverPermission
                                      ? "bg-teal-600 hover:bg-teal-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </DialogTrigger>
                            <DetailDriverDialog />
                          </Dialog>

                          <Dialog>
                            <DialogTrigger asChild>
                              <button
                                title="Editar"
                                disabled={!hasEditDriverPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Pencil
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasEditDriverPermission
                                      ? "bg-amber-600 hover:bg-amber-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </DialogTrigger>
                            <UpdateDriverDialog />
                          </Dialog>

                          <Dialog>
                            <DialogTrigger asChild>
                              <Truck className="w-7 h-7 p-1 border rounded-md text-white bg-sky-700 hover:bg-sky-700/75 cursor-pointer" />
                            </DialogTrigger>
                            <VinculateVehicleToDriverDialog />
                          </Dialog>

                          <Dialog>
                            <DialogTrigger asChild>
                              <button
                                title="Remover"
                                disabled={!hasRemoveDriverPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Trash2
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasRemoveDriverPermission
                                      ? "bg-red-600 hover:bg-red-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </DialogTrigger>
                            <DeleteDriverDialog />
                          </Dialog>
                        </div>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </div>
        </TabsContent>
        <TabsContent value="users">
          <div>
            <div className="flex items-center justify-between mt-8">
              <h1 className="text-xl text-white font-medium">Usuários</h1>
              <Dialog>
                <DialogTrigger asChild>
                  <Button className="text-white bg-green-700 hover:bg-green-700/75">
                    <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
                    <h1 className="text-base">Novo usuário</h1>
                  </Button>
                </DialogTrigger>
                <CreateUserDialog />
              </Dialog>
            </div>
            <div className="flex items-center justify-between">
              <Input
                placeholder="Digite sua busca aqui ..."
                className="w-4/12 my-4"
              />
              <Button className="text-white bg-sky-700 hover:bg-sky-700/75">
                <Download className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base">Baixar CSV</h1>
              </Button>
            </div>
            <Table>
              <TableHeader>
                <TableHead className="text-base font-bold w-1/6">
                  Usuário
                </TableHead>
                <TableHead className="text-base font-bold w-1/6">
                  Nome
                </TableHead>
                <TableHead className="text-base font-bold w-2/6">
                  E-mail
                </TableHead>
                <TableHead className="text-base font-bold w-1/6">
                  Nivel de Acesso
                </TableHead>
                <TableHead className="text-base font-bold w-1/6">
                  Telefone
                </TableHead>
                <TableHead className="text-base font-bold w-1/12">
                  Ações
                </TableHead>
              </TableHeader>
              <TableBody>
                {company &&
                  company.user &&
                  company.user.map((user: User) => (
                    <TableRow key={user.id}>
                      <TableCell className="text-base">
                        {user.username}
                      </TableCell>
                      <TableCell className="text-base">{user.name}</TableCell>
                      <TableCell className="text-base">{user.email}</TableCell>
                      <TableCell className="text-base">
                        {parseAccessLevel(user.accessLevel)}
                      </TableCell>
                      <TableCell className="text-base">
                        {formatPhoneNumber(user.phone)}
                      </TableCell>
                      <TableCell className="text-base">
                        <div className="flex items-center gap-1">
                          <Dialog>
                            <DialogTrigger asChild>
                              <Eye className="w-7 h-7 p-1 border rounded-md text-white bg-teal-600 hover:bg-teal-600/75 cursor-pointer" />
                            </DialogTrigger>
                            <DetailUserDialog />
                          </Dialog>

                          <Dialog>
                            <DialogTrigger asChild>
                              <Pencil className="w-7 h-7 p-1 border rounded-md text-white bg-amber-600 hover:bg-amber-600/75 cursor-pointer" />
                            </DialogTrigger>
                            <UpdateUserDialog />
                          </Dialog>

                          <Dialog>
                            <DialogTrigger asChild>
                              <Trash2 className="w-7 h-7 p-1 border rounded-md text-white bg-red-600 hover:bg-red-600/75 cursor-pointer" />
                            </DialogTrigger>
                            <DeleteUserDialog />
                          </Dialog>
                        </div>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </div>
        </TabsContent>
      </Tabs>
    </>
  );
}
