import axiosInstance from "@/api/axios";
import { CompanyFilters } from "@/components/company/company-filters";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
  Pagination,
  PaginationContent,
  PaginationEllipsis,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
  Dialog,
  DialogTrigger,
  Button,
  Separator,
  AlertDialog,
  AlertDialogTrigger,
} from "@/components/ui";
import { CreatePartialUserDialog } from "@/components/user/create-partial-user-dialog";
import { DeleteUserDialog } from "@/components/user/delete-user-dialog";
import { UpdateUserDialog } from "@/components/user/update-user-dialog";
import { useQuery } from "@tanstack/react-query";
import { PlusCircle, Download, Pencil, Trash2, Bolt } from "lucide-react";

import React from "react";

interface User {
  id: string;
  name: string;
  username: string;
  email: string;
}

export function UserPage() {
  const [userIdEdit, setUserIdEdit] = React.useState<null | string>(null);

  const listUsers = async () => {
    const response = await axiosInstance.get("/user");
    return response.data;
  };

  const { data: users } = useQuery({
    queryKey: ["list-users"],
    queryFn: listUsers,
  });

  return (
    <div className="max-w-full space-y-4">
      <div className="flex items-center justify-between">
        <h1 className="text-3xl font-bold">Usuários</h1>
        <Dialog>
          <DialogTrigger asChild>
            <Button className="text-white bg-green-700 hover:bg-green-700/75">
              <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base ">Novo usuário</h1>
            </Button>
          </DialogTrigger>

          <CreatePartialUserDialog />
        </Dialog>
      </div>
      <div className="flex items-center justify-between">
        <CompanyFilters />
        <Button className="text-white bg-sky-700 hover:bg-sky-700/75">
          <Download className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base ">Baixar CSV</h1>
        </Button>
      </div>

      <div className="border rounded-lg p-1">
        <div className="flex flex-col">
          <Table>
            <TableHeader>
              <TableHead className="text-base font-bold w-1/6">
                Usuário
              </TableHead>
              <TableHead className="text-base font-bold w-1/4">Nome</TableHead>
              <TableHead className="text-base font-bold w-1/3">
                E-mail
              </TableHead>
              <TableHead className="text-base font-bold w-1/12">
                Ações
              </TableHead>
            </TableHeader>
            <TableBody>
              {users &&
                users.map((user: User) => {
                  return (
                    <TableRow key={user.id}>
                      <TableCell className="text-base">
                        {user.username}
                      </TableCell>
                      <TableCell className="text-base">{user.name}</TableCell>
                      <TableCell className="text-base">{user.email}</TableCell>
                      <TableCell className="text-base">
                        <div className="flex items-center gap-1">
                          <Dialog>
                            <DialogTrigger asChild>
                              <button title="Editar permissões">
                                <Bolt className="w-7 h-7 p-1 border rounded-md text-white bg-lime-600 hover:bg-lime-600/75 cursor-pointer" />
                              </button>
                            </DialogTrigger>
                          </Dialog>
                          <Dialog>
                            <DialogTrigger asChild>
                              <button title="Editar usuário">
                                <Pencil
                                  onClick={() => setUserIdEdit(user.id)}
                                  className="w-7 h-7 p-1 border rounded-md text-white bg-amber-600 hover:bg-amber-600/75 cursor-pointer"
                                />
                              </button>
                            </DialogTrigger>
                          </Dialog>
                          <AlertDialog>
                            <AlertDialogTrigger asChild>
                              <button title="Remover usuário">
                                <Trash2 className="w-7 h-7 p-1 border rounded-md text-white bg-red-600 hover:bg-red-600/75 cursor-pointer" />
                              </button>
                            </AlertDialogTrigger>

                            <DeleteUserDialog userId={user.id} />
                          </AlertDialog>
                        </div>
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
          <div>
            <Separator />
            <Pagination className="flex justify-end">
              <PaginationContent>
                <PaginationItem>
                  <PaginationPrevious href="#" />
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">1</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationEllipsis />
                </PaginationItem>
                <PaginationItem>
                  <PaginationNext href="#" />
                </PaginationItem>
              </PaginationContent>
            </Pagination>
          </div>
          <Dialog
            onOpenChange={(state) => !state && setUserIdEdit(null)}
            open={!!userIdEdit}
          >
            <UpdateUserDialog userId={userIdEdit!} />
          </Dialog>
        </div>
      </div>
    </div>
  );
}
