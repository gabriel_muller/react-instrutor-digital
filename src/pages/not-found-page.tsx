import { Button } from "@/components/ui/button";
import { CircleArrowLeft } from "lucide-react";
import { useNavigate } from "react-router-dom";

export function NotFoundPage() {
  const navigate = useNavigate();

  const handleGoToLogin = () => {
    navigate("/login");
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-gray-900 p-4">
      <img
        src="/page-not-found.svg"
        alt="Página não encontrada"
        className="mb-8 w-1/3 max-h-80"
      />
      <p className="text-3xl font-bold text-gray-100">
        Oops... Página não encontrada
      </p>
      <p className="text-lg text-gray-400 mt-4">
        Parece que você se perdeu. Vamos ajudá-lo a voltar para casa.
      </p>
      <Button
        onClick={handleGoToLogin}
        className="text-white bg-[#F55139]/70 hover:bg-[#F55139]/60 my-8 flex items-center"
      >
        <CircleArrowLeft className="w-5 h-5 mr-2 mb-[2px]" />
        <span className="text-lg">Voltar para tela de login</span>
      </Button>
    </div>
  );
}
