import { useState } from "react";
import { useQuery } from "@tanstack/react-query";
import { useNavigate, useLocation } from "react-router-dom";
import { PlusCircle, Download, Eye, Pencil, Trash2 } from "lucide-react";
import { CreateCompanyDialog } from "@/components/company/create-company-dialog";
import { DeleteCompanyDialog } from "@/components/company/delete-company-dialog";
import { UpdateCompanyDialog } from "@/components/company/update-company-dialog";
import { AlertDialog, AlertDialogTrigger } from "@/components/ui/alert-dialog";
import { Button } from "@/components/ui/button";
import { Dialog, DialogTrigger } from "@/components/ui/dialog";
import {
  Pagination,
  PaginationContent,
  PaginationEllipsis,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from "@/components/ui/pagination";
import { Separator } from "@/components/ui/separator";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { UF } from "@/shared/enums/state-enum";
import { formatCNPJ } from "@/shared/functions/format-cnpj";
import { Spinner } from "@/components/ui/spinner";
import { Checkbox } from "@/components/ui/checkbox";
import FilterComponent from "@/components/company-filters";
import axiosInstance from "@/api/axios";
import { usePermissions } from "@/contexts/auth-context";

interface Company {
  id: string;
  cnpj: string;
  socialReason: string;
  address: string;
  addressNumber: string;
  city: {
    name: string;
    state: UF;
  };
  headOfficeCompany: {
    cnpj: string;
  };
  headOffice: boolean;
}

export function CompanyPage() {
  const [companyIdEdit, setCompanyIdEdit] = useState<null | string>(null);
  const [filters, setFilters] = useState({
    type: "",
    cnpj: "",
    socialReason: "",
  });
  const navigate = useNavigate();
  const location = useLocation();
  const { roles } = usePermissions();

  const hasCreateCompanyPermission = [
    "global_admin",
    "company_admin",
    "company_create",
  ].some((role) => roles.includes(role));

  const hasViewCompanyPermission = [
    "global_admin",
    "company_admin",
    "company_view",
  ].some((role) => roles.includes(role));

  const hasEditCompanyPermission = [
    "global_admin",
    "company_admin",
    "company_edit",
  ].some((role) => roles.includes(role));

  const hasRemoveCompanyPermission = [
    "global_admin",
    "company_admin",
    "company_delete",
  ].some((role) => roles.includes(role));

  const listCompanies = async () => {
    const response = await axiosInstance.get(`/company${location.search}`);
    return response.data;
  };

  const { data: companies, isLoading } = useQuery({
    queryKey: ["list-companies", location.search],
    queryFn: listCompanies,
  });

  const applyFilters = () => {
    const queryParams = new URLSearchParams();

    if (filters.type) queryParams.append("type", filters.type);
    if (filters.cnpj) queryParams.append("cnpj", filters.cnpj);
    if (filters.socialReason)
      queryParams.append("socialReason", filters.socialReason);

    navigate({
      pathname: "/empresa",
      search: queryParams.toString(),
    });
  };

  const clearFilters = () => {
    setFilters({ type: "", cnpj: "", socialReason: "" });
    navigate("/empresa");
  };

  return (
    <div className="max-w-full space-y-4">
      <div className="flex items-center justify-between">
        <h1 className="text-3xl font-bold">Empresas</h1>
        <Dialog>
          <DialogTrigger asChild>
            <Button
              className={`text-white ${
                hasCreateCompanyPermission
                  ? "bg-green-700 hover:bg-green-700/75"
                  : "bg-gray-400 cursor-not-allowed"
              } disabled:cursor-not-allowed`}
              disabled={!hasCreateCompanyPermission}
            >
              <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base">Nova empresa</h1>
            </Button>
          </DialogTrigger>
          <CreateCompanyDialog />
        </Dialog>
      </div>
      <div className="flex items-center justify-end">
        <Button className="text-white bg-sky-700 hover:bg-sky-700/75">
          <Download className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base">Baixar CSV</h1>
        </Button>
      </div>
      <FilterComponent
        filters={filters}
        setFilters={setFilters}
        applyFilters={applyFilters}
        clearFilters={clearFilters}
      />

      {isLoading ? (
        <div className="flex justify-center items-center py-10">
          <Spinner size={"large"} />
        </div>
      ) : (
        <div className="border rounded-lg p-1">
          <div className="flex flex-col">
            <Table>
              <TableHeader>
                <TableHead className="text-base font-bold w-1/12">ID</TableHead>
                <TableHead className="text-base font-bold w-1/6">
                  CNPJ
                </TableHead>
                <TableHead className="text-base font-bold w-1/5">
                  Razão Social
                </TableHead>
                <TableHead className="text-base font-bold w-1/4">
                  Endereço
                </TableHead>
                <TableHead className="text-base font-bold w-1/12">
                  Filial
                </TableHead>
                <TableHead className="text-base font-bold w-1/6">
                  CNPJ Matriz
                </TableHead>
                <TableHead className="text-base font-bold w-1/12">
                  Ações
                </TableHead>
              </TableHeader>
              <TableBody>
                {companies &&
                  companies.map((company: Company) => (
                    <TableRow key={company.id}>
                      <TableCell className="text-base">{company.id}</TableCell>
                      <TableCell className="text-base">
                        {formatCNPJ(company.cnpj)}
                      </TableCell>
                      <TableCell className="text-base">
                        {company.socialReason}
                      </TableCell>
                      <TableCell className="text-base">{`${company.address}, ${company.addressNumber}, ${company.city.name} - ${company.city.state}`}</TableCell>
                      <TableCell className="text-base">
                        <Checkbox
                          checked={!company.headOffice}
                          className="cursor-not-allowed"
                        />
                      </TableCell>
                      <TableCell className="text-base">
                        {company.headOfficeCompany
                          ? formatCNPJ(company.headOfficeCompany.cnpj)
                          : null}
                      </TableCell>
                      <TableCell className="text-base">
                        <div className="flex items-center gap-1">
                          <Dialog>
                            <DialogTrigger asChild>
                              <button
                                title="Visualizar"
                                disabled={!hasViewCompanyPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Eye
                                  onClick={() =>
                                    navigate(`/empresa/${company.id}`)
                                  }
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasViewCompanyPermission
                                      ? "bg-teal-600 hover:bg-teal-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </DialogTrigger>
                          </Dialog>
                          <Dialog>
                            <DialogTrigger asChild>
                              <button
                                title="Editar"
                                disabled={!hasEditCompanyPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Pencil
                                  onClick={() => setCompanyIdEdit(company.id)}
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasEditCompanyPermission
                                      ? "bg-amber-600 hover:bg-amber-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </DialogTrigger>
                          </Dialog>
                          <AlertDialog>
                            <AlertDialogTrigger asChild>
                              <button
                                title="Remover"
                                disabled={!hasRemoveCompanyPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Trash2
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasRemoveCompanyPermission
                                      ? "bg-red-600 hover:bg-red-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </AlertDialogTrigger>
                            <DeleteCompanyDialog companyId={company.id} />
                          </AlertDialog>
                        </div>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
            <div>
              <Separator />
              <Pagination className="flex justify-end">
                <PaginationContent>
                  <PaginationItem>
                    <PaginationPrevious href="#" />
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink href="#">1</PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationEllipsis />
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationNext href="#" />
                  </PaginationItem>
                </PaginationContent>
              </Pagination>
            </div>
            <Dialog
              onOpenChange={(state) => !state && setCompanyIdEdit(null)}
              open={!!companyIdEdit}
            >
              <UpdateCompanyDialog companyId={companyIdEdit!} />
            </Dialog>
          </div>
        </div>
      )}
    </div>
  );
}
