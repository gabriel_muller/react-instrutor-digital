/* eslint-disable @typescript-eslint/no-explicit-any */
import axiosInstance from "@/api/axios";
import { CompanyFilters } from "@/components/company/company-filters";
import { CreateEquipmentDialog } from "@/components/equipment/create-equipment-dialog";
import { DeleteEquipmentDialog } from "@/components/equipment/delete-equipment-dialog";
import { DetailEquipmentDialog } from "@/components/equipment/detail-equipment-dialog";
import { UpdateEquipmentDialog } from "@/components/equipment/update-equipment-dialog";
import { AlertDialog, AlertDialogTrigger } from "@/components/ui/alert-dialog";
import { Button } from "@/components/ui/button";
import { Dialog, DialogTrigger } from "@/components/ui/dialog";
import {
  Pagination,
  PaginationContent,
  PaginationEllipsis,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from "@/components/ui/pagination";
import { Separator } from "@/components/ui/separator";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { usePermissions } from "@/contexts/auth-context";
import { formatDate } from "@/shared/functions/format-date";
import { useQuery } from "@tanstack/react-query";
import { PlusCircle, Download, Eye, Pencil, Trash2 } from "lucide-react";

import React from "react";

interface Equipment {
  id: string;
  identifier: string;
  serialNumber: string;
  batch: string;
  manufacturingDate: string;
  createdAt: Date;
  updatedAt: Date;
}

export function EquipmentPage() {
  const [equipmentIdEdit, setEquipmentIdEdit] = React.useState<null | string>(
    null
  );
  const { roles } = usePermissions();

  const hasCreateEquipmentPermission = [
    "global_admin",
    "company_admin",
    "equipament_create",
  ].some((role) => roles.includes(role));

  const hasViewEquipmentPermission = [
    "global_admin",
    "company_admin",
    "equipament_view",
  ].some((role) => roles.includes(role));

  const hasEditEquipmentPermission = [
    "global_admin",
    "company_admin",
    "equipament_edit",
  ].some((role) => roles.includes(role));

  const hasRemoveEquipmentPermission = [
    "global_admin",
    "company_admin",
    "equipament_delete",
  ].some((role) => roles.includes(role));

  const listEquipments = async () => {
    const response = await axiosInstance.get("/equipament");
    return response.data;
  };

  const { data: equipments } = useQuery({
    queryKey: ["list-equipments"],
    queryFn: listEquipments,
  });

  return (
    <div className="max-w-full space-y-4">
      <div className="flex items-center justify-between">
        <h1 className="text-3xl font-bold">Equipamentos</h1>
        <Dialog>
          <DialogTrigger asChild>
            <Button
              className={`text-white ${
                hasCreateEquipmentPermission
                  ? "bg-green-700 hover:bg-green-700/75"
                  : "bg-gray-400 cursor-not-allowed"
              } disabled:cursor-not-allowed`}
            >
              <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base ">Novo equipamento</h1>
            </Button>
          </DialogTrigger>

          <CreateEquipmentDialog />
        </Dialog>
      </div>
      <div className="flex items-center justify-between">
        <CompanyFilters />
        <Button className="text-white bg-sky-700 hover:bg-sky-700/75">
          <Download className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base ">Baixar CSV</h1>
        </Button>
      </div>

      <div className="border rounded-lg p-1">
        <div className="flex flex-col">
          <Table>
            <TableHeader>
              <TableHead className="text-base font-bold w-1/6">Nome</TableHead>
              <TableHead className="text-base font-bold w-1/6">
                Número de série
              </TableHead>
              <TableHead className="text-base font-bold w-1/6">Lote</TableHead>
              <TableHead className="text-base font-bold w-1/5">
                Data de fabricação
              </TableHead>
              <TableHead className="text-base font-bold w-1/12">
                Ações
              </TableHead>
            </TableHeader>
            <TableBody>
              {equipments &&
                equipments.map((equipment: Equipment) => {
                  return (
                    <TableRow key={equipment.id}>
                      <TableCell className="text-base">
                        {equipment.identifier}
                      </TableCell>
                      <TableCell className="text-base">
                        {equipment.serialNumber}
                      </TableCell>
                      <TableCell className="text-base">
                        {equipment.batch}
                      </TableCell>
                      <TableCell className="text-base">
                        {formatDate(equipment.manufacturingDate)}
                      </TableCell>
                      <TableCell className="text-base">
                        <div className="flex items-center gap-1">
                          <Dialog>
                            <DialogTrigger asChild>
                              <button
                                title="Visualizar"
                                disabled={!hasViewEquipmentPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Eye
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasViewEquipmentPermission
                                      ? "bg-teal-600 hover:bg-teal-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </DialogTrigger>
                            <DetailEquipmentDialog equipmentId={equipment.id} />
                          </Dialog>
                          <Dialog>
                            <DialogTrigger asChild>
                              <button
                                title="Editar"
                                disabled={!hasEditEquipmentPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Pencil
                                  onClick={() =>
                                    setEquipmentIdEdit(equipment.id)
                                  }
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasEditEquipmentPermission
                                      ? "bg-amber-600 hover:bg-amber-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </DialogTrigger>
                          </Dialog>
                          <AlertDialog>
                            <AlertDialogTrigger asChild>
                              <button
                                title="Remover"
                                disabled={!hasRemoveEquipmentPermission}
                                className="disabled:cursor-not-allowed"
                              >
                                <Trash2
                                  className={`w-7 h-7 p-1 border rounded-md text-white ${
                                    hasRemoveEquipmentPermission
                                      ? "bg-red-600 hover:bg-red-600/75"
                                      : "bg-gray-400 cursor-not-allowed"
                                  }`}
                                />
                              </button>
                            </AlertDialogTrigger>
                            <DeleteEquipmentDialog equipmentId={equipment.id} />
                          </AlertDialog>
                        </div>
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
          <div>
            <Separator />
            <Pagination className="flex justify-end">
              <PaginationContent>
                <PaginationItem>
                  <PaginationPrevious href="#" />
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">1</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationEllipsis />
                </PaginationItem>
                <PaginationItem>
                  <PaginationNext href="#" />
                </PaginationItem>
              </PaginationContent>
            </Pagination>
          </div>
          <Dialog
            onOpenChange={(state) => !state && setEquipmentIdEdit(null)}
            open={!!equipmentIdEdit}
          >
            <UpdateEquipmentDialog equipmentId={equipmentIdEdit!} />
          </Dialog>
        </div>
      </div>
    </div>
  );
}
