import {
  Form,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
} from "@/components/ui/form";
import { zodResolver } from "@hookform/resolvers/zod";
import axios from "axios";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { Input } from "@/components/ui/input";
import { LoadingButton } from "@/components/ui/loading-button";
import { LogIn, Eye, EyeOff } from "lucide-react";
import { useState } from "react";
import { useToast } from "@/components/ui/use-toast";
import { useNavigate } from "react-router-dom";
import { envConfig } from "../../env-config";
import { usePermissions } from "@/contexts/auth-context";

const loginSchema = z.object({
  username: z.string(),
  password: z.string(),
});

type LoginProps = {
  username: string;
  password: string;
};

type LoginResponse = {
  access_token: string;
  expires_in: string;
  refresh_token: string;
};

type LoginSchema = z.infer<typeof loginSchema>;

export function LoginPage() {
  const form = useForm<LoginSchema>({
    resolver: zodResolver(loginSchema),
  });

  const { toast } = useToast();
  const navigate = useNavigate();
  const { setToken } = usePermissions();

  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleLogin = async (data: LoginProps) => {
    setLoading(true);
    const { username, password } = data;
    const token = btoa(`${username}:${password}`);
    try {
      const response = await axios.post<LoginResponse>(
        `${envConfig.baseUrl}/auth/authenticate`,
        {},
        {
          headers: {
            Authorization: `Basic ${token}`,
          },
        }
      );

      const accessToken = response.data.access_token;

      localStorage.setItem("access_token", accessToken);
      localStorage.setItem("expires_in", response.data.expires_in);
      localStorage.setItem("refresh_token", response.data.refresh_token);

      setToken(accessToken);

      toast({
        title: "Login efetuado com sucesso!",
        description: `Logo vamos redirecioná-lo para a tela principal.`,
        className: "bg-green-700 border-none antialiasing",
        duration: 1200,
      });
      setTimeout(() => {
        navigate("/inicio");
      }, 1400);
      return response.data;
    } catch (error) {
      toast({
        title: "Credenciais inválidas!",
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
        description: `Verifique suas credenciais e tente novamente.`,
        duration: 3000,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-slate-600 p-4">
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleLogin)}
          className="space-y-10 p-10 shadow-md rounded-md w-full max-w-xl bg-slate-950 flex flex-col items-center"
        >
          <img src="/logo-instrutor-digital.svg" className="self-center my-8" />
          <FormDescription className="text-xl font-semibold text-white text-center">
            Seja bem vindo!
          </FormDescription>
          <div className="w-full">
            <FormField
              control={form.control}
              name="username"
              render={({ field }) => (
                <FormItem className="flex flex-col w-full">
                  <FormLabel>
                    Nome de Usuário<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome de usuário"
                      {...field}
                      className="w-full"
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="w-full relative">
            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormItem className="flex flex-col w-full">
                  <FormLabel>
                    Senha<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <div className="relative w-full">
                      <Input
                        placeholder="Digite a senha"
                        type={showPassword ? "text" : "password"}
                        {...field}
                        className="w-full pr-10"
                      />
                      <div
                        className="absolute inset-y-0 right-0 pr-3 flex items-center cursor-pointer"
                        onClick={togglePasswordVisibility}
                      >
                        {showPassword ? (
                          <EyeOff className="text-gray-400 w-4 h-4" />
                        ) : (
                          <Eye className="text-gray-400 w-4 h-4" />
                        )}
                      </div>
                    </div>
                  </FormControl>
                  <FormMessage />
                  <div className="flex justify-end mt-2">
                    <a
                      href="/esqueceu-a-senha"
                      className="text-sm text-blue-500 hover:underline"
                    >
                      Esqueceu a senha?
                    </a>
                  </div>
                </FormItem>
              )}
            />
          </div>
          <div className="flex justify-center w-full mt-3">
            <LoadingButton
              type="submit"
              loading={loading}
              className="w-full text-white bg-blue-700 hover:bg-blue-800 mb-4"
            >
              <LogIn className="w-4 h-4 mr-2" />
              Entrar
            </LoadingButton>
          </div>
        </form>
      </Form>
    </div>
  );
}
