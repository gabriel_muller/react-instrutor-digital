import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import * as React from "react";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import axios from "axios";
import { CircleCheck } from "lucide-react";
// @ts-expect-error
import InputMask from "react-input-mask";
import { getCityIdByName } from "@/shared/requests/get-city-id-by-name";
import { UF } from "@/shared/enums/state-enum";
import { useToast } from "@/components/ui/use-toast";
import { getAddressByZipCode } from "@/shared/requests/get-address-by-zipcode";
import { LoadingButton } from "@/components/ui/loading-button";
import {
  Form,
  FormControl,
  FormField,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { StateComboboxFilters } from "@/components/state-combobox-filters";
import { useLocation } from "react-router-dom";
import { envConfig } from "../../env-config";
import {
  cleanCEP,
  cleanPhoneNumber,
  formatAccessLevel,
  formatPhoneNumber,
  parseAccessLevel,
} from "@/shared/functions";
import { Input, Separator } from "@/components/ui";

const userRegisterSchema = z.object({
  name: z.string(),
  username: z.string(),
  email: z.string(),
  function: z.string().nullable().optional(),
  password: z.string(),
  registry: z.string().nullable().optional(),
  zipcode: z.string(),
  address: z.string(),
  addressNumber: z.string(),
  addressComp: z.string().nullable().optional(),
  addressCity: z.string(),
  addressUf: z.string(),
  phone: z.string(),
  phoneSecondary: z.string().nullable().optional(),
  accessLevel: z.string(),
  birthdate: z.any(),
});

type UserRegisterProps = {
  name: string;
  username: string;
  email: string;
  function?: string | null;
  password: string;
  registry?: string | null;
  zipcode: string;
  address: string;
  addressNumber: string;
  addressComp?: string | null;
  addressCity?: string;
  addressCityId: number;
  addressUf: UF;
  phone: string;
  phoneSecondary?: string | null;
  accessLevel: string;
  birthdate: Date | string;
  companyId: number;
  userGroupId: number;
};

type FindPreRegisterByToken = {
  name: string;
  email: string;
  phone: string;
  accessLevel: string;
};

type UserRegisterSchema = z.infer<typeof userRegisterSchema>;

export function UserRegisterPage() {
  const [_selectedAddressState, setSelectedAddressState] = React.useState();
  const [isLoading, setIsLoading] = React.useState(false);

  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const token = queryParams.get("token");

  const queryClient = useQueryClient();
  const { toast } = useToast();

  const form = useForm<UserRegisterSchema>({
    resolver: zodResolver(userRegisterSchema),
  });

  const handleZipCodeChange = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const zipcode = event.target.value.replace(/\D/g, "");
    if (zipcode.length === 8) {
      const address = await getAddressByZipCode(zipcode);
      if (address) {
        form.setValue("address", address.logradouro || "");
        form.setValue("addressCity", address.localidade || "");
        form.setValue("addressUf", address.uf || "");
        form.setValue("addressNumber", "");
        form.setValue("addressComp", "");
      }
    }
  };

  const findPreRegisterByToken = async (token: string | null) => {
    if (!token) return {};
    const response = await axios.get<FindPreRegisterByToken>(
      `${envConfig.baseUrl}/user/register/${token}`
    );
    console.log(response.data);
    const data = response.data;

    form.setValue("name", data.name);
    form.setValue("email", data.email);
    form.setValue("phone", data.phone);
    form.setValue("accessLevel", parseAccessLevel(data.accessLevel));

    return data;
  };

  const userRegister = async (data: UserRegisterProps) => {
    try {
      const response = await axios.post<UserRegisterProps>(
        `${envConfig.baseUrl}/user/${token}`,
        data
      );
      return response.data;
    } catch (error) {
      throw new Error("Erro ao criar empresa");
    }
  };

  const { mutateAsync: userRegisterFn } = useMutation({
    mutationFn: userRegister,
    onSuccess(_data, variables) {
      queryClient.setQueryData(["list-users"], (existingUsers: any) => [
        ...(existingUsers || []),
        {
          name: variables.name,
          username: variables.username,
          email: variables.email,
          function: variables.function,
          password: variables.password,
          registry: variables.registry,
          zipcode: variables.zipcode,
          address: variables.address,
          addressNumber: variables.addressNumber,
          addressComp: variables.addressComp,
          addressCity: variables.addressCity,
          addressUf: variables.addressUf,
          phone: variables.phone,
          phoneSecondary: variables.phoneSecondary,
        },
      ]);
      setIsLoading(false);
      toast({
        title: "Usuário criado com sucesso!",
        description: `O usuário ${variables.username} foi criado`,
        duration: 3000,
      });
    },
    onError(_error) {
      setIsLoading(false);
      toast({
        title: "Erro!",
        description: `Não foi possível criar o usuário.`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    },
  });

  useQuery({
    queryKey: ["find-preregister-by-token", token],
    queryFn: () => findPreRegisterByToken(token),
    enabled: !!token,
    initialData: {},
  });

  async function handleUserRegister(data: UserRegisterSchema) {
    const addressCityId = await getCityIdByName(
      data.addressCity ? data.addressCity : ""
    );
    setIsLoading(true);
    try {
      console.log(formatPhoneNumber(data.phone));
      await userRegisterFn({
        name: data.name,
        username: data.username,
        email: data.email,
        function: data.function ? data.function : null,
        password: data.password,
        registry: data.registry ? data.registry : null,
        zipcode: cleanCEP(data.zipcode),
        address: data.address,
        addressNumber: data.addressNumber,
        addressComp: data.addressComp ? data.addressComp : undefined,
        addressCityId: addressCityId,
        addressUf: data.addressUf as UF,
        phone: cleanPhoneNumber(data.phone),
        phoneSecondary: data.phoneSecondary
          ? cleanPhoneNumber(data.phoneSecondary)
          : null,
        accessLevel: formatAccessLevel(data.accessLevel),
        birthdate: new Date(data.birthdate).toISOString(),
        companyId: 1,
        userGroupId: 1,
      });
    } catch (error) {
      setIsLoading(false);
      toast({
        title: "Erro!",
        description: `Não foi possível criar o usário ${data.username}`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <div className="min-h-screen flex items-center justify-center bg-slate-600 p-4">
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleUserRegister)}
          className="space-y-8 p-6 shadow-md rounded-md w-full max-w-7xl bg-slate-950"
        >
          <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6 mb-10 border-zinc-100">
            <FormDescription className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4 text-2xl font-semibold text-white">
              Confirmar novo usuário
            </FormDescription>
            <Separator className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4" />
            <FormDescription className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4 text-lg font-semibold">
              Usuário
            </FormDescription>
            <Separator className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4" />

            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>
                    Nome<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input placeholder="Digite o nome" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="username"
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>
                    Nome de Usuário<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input placeholder="Digite o nome de usuário" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="email"
              disabled={true}
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>
                    E-mail<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input placeholder="Digite o e-mail" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>
                    Senha<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a senha"
                      type="password"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="registry"
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>Número Identificador / Matrícula</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a matrícula"
                      {...field}
                      value={field.value ?? ""}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="birthdate"
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>
                    Data de Nascimento<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      type="date"
                      className="text-slate-400"
                      {...(field as any)}
                      {...form.register("birthdate")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="function"
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>Função / Departamento</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a função"
                      {...field}
                      value={field.value ?? ""}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="accessLevel"
              disabled={true}
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>
                    Nível de Acesso<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input placeholder="Digite a função" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>

          <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6">
            <FormDescription className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4 text-lg font-semibold">
              Logradouro
            </FormDescription>
            <Separator className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4" />
            <FormField
              control={form.control}
              name="zipcode"
              render={({ field }) => (
                <FormItem className="col-span-1 sm:col-span-1 flex flex-col">
                  <FormLabel>
                    CEP<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <InputMask
                      mask="99999-999"
                      placeholder="Digite o CEP"
                      {...field}
                      {...form.register("zipcode")}
                      onChange={handleZipCodeChange}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="address"
              render={({ field }) => (
                <FormItem className="col-span-1 sm:col-span-2 lg:col-span-1 flex flex-col">
                  <FormLabel>
                    Endereço<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input placeholder="Digite o endereço" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressNumber"
              render={({ field }) => (
                <FormItem className="col-span-1 sm:col-span-1 lg:col-span-1 flex flex-col">
                  <FormLabel>
                    Número<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input placeholder="Digite o número" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="addressComp"
              render={({ field }) => (
                <FormItem className="col-span-1 sm:col-span-2 lg:col-span-1 flex flex-col">
                  <FormLabel>Complemento</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o complemento"
                      value={field.value ?? ""}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressUf"
              render={({ field }) => (
                <FormItem className="col-span-1 sm:col-span-1 lg:col-span-1 flex flex-col">
                  <FormLabel>
                    Estado<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <StateComboboxFilters
                      onSelect={(selectedValue) => {
                        setSelectedAddressState(selectedValue as any);
                        form.setValue("addressUf", selectedValue);
                      }}
                      {...field}
                      {...form.register("addressUf")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="addressCity"
              render={({ field }) => (
                <FormItem className="col-span-1 sm:col-span-2 lg:col-span-1 flex flex-col">
                  <FormLabel>
                    Cidade<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a cidade"
                      value={field.value ?? ""}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6">
            <FormDescription className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4 text-lg font-semibold">
              Contato
            </FormDescription>
            <Separator className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4" />

            <FormField
              control={form.control}
              name="phone"
              render={({ field }) => (
                <FormItem className="col-span-1 sm:col-span-1 lg:col-span-1 flex flex-col">
                  <FormLabel>
                    Telefone Principal<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone"
                      {...field}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="phoneSecondary"
              render={({ field }) => (
                <FormItem className="col-span-1 sm:col-span-1 lg:col-span-1 flex flex-col">
                  <FormLabel>Telefone Secundário</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone secundário"
                      {...field}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6">
            <Separator className="col-span-1 sm:col-span-2 lg:col-span-3 xl:col-span-4" />
          </div>
          <div className="flex justify-end space-x-4 mt-4">
            <LoadingButton
              loading={isLoading}
              type="submit"
              className="text-white bg-green-600 hover:bg-green-700"
            >
              <CircleCheck className="w-4 h-4 mr-2" />
              Confirmar cadastro
            </LoadingButton>
          </div>
        </form>
      </Form>
    </div>
  );
}
