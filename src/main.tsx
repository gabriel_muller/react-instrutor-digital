import ReactDOM from "react-dom/client";
import "./index.css";
import { ThemeProvider } from "./contexts/theme-provider.tsx";
import Layout from "./layout.tsx";
import { createBrowserRouter, RouterProvider, Outlet } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { Toaster } from "./components/ui/toaster.tsx";
import {
  CompanyDetailPage,
  CompanyPage,
  EquipmentPage,
  GasEmissionPage,
  HomePage,
  LoginPage,
  ScorePage,
  UserPage,
  ResetPasswordPage,
  UserRegisterPage,
  FuelConsumptionPage,
  NotFoundPage,
  AccessDeniedPage,
} from "./pages/index.ts";
import { ProtectedRoute } from "./routes/protected-route.tsx";
import { PermissionsProvider } from "./contexts/auth-context.tsx";

const LayoutWrapper = () => (
  <Layout>
    <Outlet />
  </Layout>
);

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <ProtectedRoute>
        <LayoutWrapper />
      </ProtectedRoute>
    ),
    children: [
      { path: "inicio", element: <HomePage /> },
      { path: "consumo-combustivel", element: <FuelConsumptionPage /> },
      { path: "pontuacao", element: <ScorePage /> },
      { path: "emissao-gases", element: <GasEmissionPage /> },
      {
        path: "empresa",
        element: (
          <ProtectedRoute requiredRoles={["company_view", "global_admin"]}>
            <CompanyPage />
          </ProtectedRoute>
        ),
      },
      {
        path: "empresa/:companyId",
        element: (
          <ProtectedRoute requiredRoles={["company_view", "global_admin"]}>
            <CompanyDetailPage />
          </ProtectedRoute>
        ),
      },
      { path: "equipamento", element: <EquipmentPage /> },
      { path: "usuario", element: <UserPage /> },
    ],
  },
  { path: "usuario/finalizar-cadastro", element: <UserRegisterPage /> },
  { path: "login", element: <LoginPage /> },
  { path: "esqueceu-a-senha", element: <ResetPasswordPage /> },
  { path: "pagina-nao-encontrada", element: <NotFoundPage /> },
  { path: "acesso-negado", element: <AccessDeniedPage /> }, // Adicione a rota de acesso negado
  { path: "*", element: <NotFoundPage /> },
]);

const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById("root")!).render(
  <QueryClientProvider client={queryClient}>
    <PermissionsProvider>
      <ThemeProvider defaultTheme="dark" storageKey="vite-ui-theme">
        <RouterProvider router={router} />
        <Toaster />
      </ThemeProvider>
    </PermissionsProvider>
  </QueryClientProvider>
);
