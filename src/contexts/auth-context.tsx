import React, { createContext, useContext, useEffect, useState } from "react";
import axiosInstance from "@/api/axios";
import { jwtDecode } from "jwt-decode";

interface PermissionsContextType {
  roles: string[];
  loading: boolean;
  setToken: (token: string | null) => void;
}

const PermissionsContext = createContext<PermissionsContextType | undefined>(
  undefined
);

export const usePermissions = () => {
  const context = useContext(PermissionsContext);
  if (!context) {
    throw new Error(
      "usePermissions deve ser usado dentro de um PermissionsProvider"
    );
  }
  return context;
};

export const PermissionsProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [roles, setRoles] = useState<string[]>([]);
  const [loading, setLoading] = useState(true);
  const [token, setToken] = useState<string | null>(
    localStorage.getItem("access_token")
  );

  const fetchUserRoles = async (token: string | null) => {
    if (token) {
      try {
        const decoded = jwtDecode<any>(token);
        const { data } = await axiosInstance.get(`/user/${decoded.sub}`);
        const userGroup = await axiosInstance.get(
          `/user-group/${data.userGroupId}`
        );
        setRoles(userGroup.data.roles);
      } catch (error) {
        console.error("Error fetching roles:", error);
        setRoles([]);
      } finally {
        setLoading(false);
      }
    } else {
      setRoles([]);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchUserRoles(token);
  }, [token]);

  return (
    <PermissionsContext.Provider value={{ roles, loading, setToken }}>
      {children}
    </PermissionsContext.Provider>
  );
};
