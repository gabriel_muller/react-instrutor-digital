import { format } from "date-fns";
import { ptBR } from "date-fns/locale";

export function formatDate(data: string): string {
  const dataObj = new Date(data);
  const dataFormatada = format(dataObj, "dd 'de' MMMM 'de' yyyy", {
    locale: ptBR,
  });
  return dataFormatada;
}

export const formatDateToDDMMYYYY = (dateString: string) => {
  const date = new Date(dateString);
  const day = String(date.getUTCDate()).padStart(2, "0");
  const month = String(date.getUTCMonth() + 1).padStart(2, "0");
  const year = date.getUTCFullYear();
  return `${day}-${month}-${year}`;
};

export const formatDateToISOString = (dateString: string) => {
  const [day, month, year] = dateString.split("-");
  const date = new Date(`${year}-${month}-${day}`);
  return date.toISOString();
};
