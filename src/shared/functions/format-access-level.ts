export function formatAccessLevel(tipoUsuario: string): string {
  const accessLevel: { [key: string]: string } = {
    "Administrador Global": "GLOBAL_ADMIN",
    "Usuário Global": "GLOBAL_USER",
    "Administrador da Empresa": "COMPANY_ADMIN",
    "Usuário da Empresa": "COMPANY_USER",
  };
  return accessLevel[tipoUsuario] || "Tipo de usuário não encontrado";
}

export function parseAccessLevel(tipoUsuario: string): string {
  const mapeamento: { [key: string]: string } = {
    GLOBAL_ADMIN: "Administrador Global",
    GLOBAL_USER: "Usuário Global",
    COMPANY_ADMIN: "Administrador da Empresa",
    COMPANY_USER: "Usuário da Empresa",
  };
  return mapeamento[tipoUsuario] || "Tipo de usuário não encontrado";
}
