export function formatCEP(cep: string) {
    const cleanedCEP = cep.replace(/\D/g, "");
  
    if (cleanedCEP.length !== 8) {
      return cep;
    }
  
    return cleanedCEP.slice(0, 5) + "-" + cleanedCEP.slice(5);
  }

export  function cleanCEP(cep: string): string {
    return cep.replace(/\D/g, '');
  }
  