export function formatCPF(cpf: string): string {
    const cleanedCPF = cpf.replace(/\D/g, "");
  
    return (
      cleanedCPF.slice(0, 3) +
      "." +
      cleanedCPF.slice(3, 6) +
      "." +
      cleanedCPF.slice(6, 9) +
      "-" +
      cleanedCPF.slice(9)
    );
  }
  