export function formatCarPlate(plate: string): string {
  const oldPlateFormat = /^[a-zA-Z]{3}[0-9]{4}$/;
  const newPlateFormat = /^[a-zA-Z]{3}[0-9][a-zA-Z][0-9]{2}$/;

  return oldPlateFormat.test(plate)
    ? plate.replace(/([a-zA-Z]{3})([0-9]{4})/, "$1-$2")
    : newPlateFormat.test(plate)
    ? plate.toUpperCase()
    : (() => {
        throw new Error("Invalid car plate format");
      })();
}
