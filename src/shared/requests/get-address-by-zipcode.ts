import axios from "axios";

export const getAddressByZipCode = async (zipcode: string) => {
  try {
    const response = await axios.get(
      `https://viacep.com.br/ws/${zipcode}/json/`
    );
    return response.data;
  } catch (error) {
    console.error("Erro ao obter endereço pelo CEP:", error);
    return null;
  }
};
