import axiosInstance from "@/api/axios";

export const getCityIdByName = async (name: string) => {
  const response = await axiosInstance.get(`/city/name/${name}`);
  return response.data.id;
};
