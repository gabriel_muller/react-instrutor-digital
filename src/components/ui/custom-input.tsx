import React from "react";
import {
  FormField,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
} from "./form";
import { Input } from "./input";
import { Control, UseFormRegister } from "react-hook-form";
import { UF } from "@/shared/enums/state-enum";

interface FormValues {
  cnpj: string;
  socialReason: string;
  fantasyName: string;
  zipcode?: string;
  address?: string;
  addressNumber?: string;
  addressComp?: string;
  addressCity?: string;
  addressUf?: UF;
  district?: string;
  city?: string;
  state?: string;
  email?: string;
  emailSecondary?: string;
  phonePrimary?: string;
  phoneSecondary?: string;
}

interface CustomInputProps {
  control: Control<FormValues>;
  name: keyof FormValues;
  label: string;
  placeholder: string;
  register: UseFormRegister<FormValues>;
  required?: boolean;
  className?: string;
}

export const CustomInput: React.FC<CustomInputProps> = ({
  control,
  name,
  label,
  placeholder,
  register,
  required,
  className,
}) => (
  <FormField
    control={control}
    name={name}
    render={({ field }) => (
      <FormItem className={`flex flex-col ${className}`}>
        <FormLabel>
          {label}
          {required && <b className="text-red-600 mx-1"> *</b>}
        </FormLabel>
        <FormControl>
          <Input placeholder={placeholder} {...field} {...register(name)} />
        </FormControl>
        <FormMessage />
      </FormItem>
    )}
  />
);
