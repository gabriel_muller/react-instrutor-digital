import { useEffect, useState } from "react";
import { jwtDecode } from "jwt-decode";
import { Button } from "@/components/ui/button";
import {
  Axe,
  BarChart3,
  Building2,
  Fan,
  Fuel,
  Home,
  User,
  LogOut,
} from "lucide-react";
import { Avatar, AvatarFallback, AvatarImage } from "./avatar";
import axiosInstance from "@/api/axios";

interface SidebarProps {
  collapsed: boolean;
}

interface JwtPayload {
  sub: string;
}

interface FindUserById {
  name: string;
  username: string;
  userGroupId: any;
}

export const Sidebar = ({ collapsed }: SidebarProps) => {
  const [username, setUsername] = useState<string | null>(null);
  const [_roles, setRoles] = useState<string[]>([]);

  useEffect(() => {
    const fetchUser = async () => {
      const token = localStorage.getItem("access_token");
      if (token) {
        try {
          const decoded = jwtDecode<JwtPayload>(token);
          const { data } = await axiosInstance.get<FindUserById>(
            `/user/${decoded.sub}`
          );
          const userGroup = await axiosInstance.get(
            `/user-group/${data.userGroupId}`
          );
          console.log(userGroup.data.roles);
          console.log(data);
          setUsername(data.name);
          setRoles(userGroup.data.roles);
        } catch (error) {
          console.error("Invalid token:", error);
          setUsername(null);
        }
      }
    };

    fetchUser();
  }, []);

  const handleNavigation = (path: string) => {
    window.location.href = path;
  };

  const handleLogout = () => {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("expires_in");
    window.location.href = "/login";
  };

  return (
    <div
      className={`h-full border-r-2 border-slate-800 transition-all duration-300 ${
        collapsed ? "w-20" : "w-64"
      }`}
    >
      <div className="p-4 flex flex-col justify-between h-full relative">
        <div>
          <div className="flex items-center justify-center mb-6 mt-5">
            {!collapsed ? (
              <img
                src="/logo-instrutor-sidebar.svg"
                alt="Logo"
                className="h-10"
              />
            ) : (
              <img
                src="/logo-instrutor-digital-colapsado.svg"
                alt="Logo Colapsado"
                className="h-10"
              />
            )}
          </div>
          <ul>
            <li className="mb-3">
              <Button
                type="button"
                title="Início"
                variant={"ghost"}
                onClick={() => handleNavigation("/inicio")}
                className="flex items-center"
              >
                <Home className="w-5 h-5" />
                {!collapsed && <span className="ml-2">Início</span>}
              </Button>
            </li>
            <li className="mb-3">
              <Button
                type="button"
                title="Consumo de combustível"
                variant={"ghost"}
                onClick={() => handleNavigation("/consumo-combustivel")}
                className="flex items-center"
              >
                <Fuel className="w-5 h-5" />
                {!collapsed && (
                  <span className="ml-2">Consumo de combustível</span>
                )}
              </Button>
            </li>
            <li className="mb-3">
              <Button
                type="button"
                title="Pontuação"
                variant={"ghost"}
                onClick={() => handleNavigation("/pontuacao")}
                className="flex items-center"
              >
                <BarChart3 className="w-5 h-5" />
                {!collapsed && <span className="ml-2">Pontuação</span>}
              </Button>
            </li>
            <li className="mb-3">
              <Button
                type="button"
                title="Emissão de gases"
                variant={"ghost"}
                onClick={() => handleNavigation("/emissao-gases")}
                className="flex items-center"
              >
                <Fan className="w-5 h-5" />
                {!collapsed && <span className="ml-2">Emissão de gases</span>}
              </Button>
            </li>
            <li className="mb-3">
              <Button
                type="button"
                title="Empresas"
                variant={"ghost"}
                onClick={() => handleNavigation("/empresa")}
                className="flex items-center"
              >
                <Building2 className="w-5 h-5" />
                {!collapsed && <span className="ml-2">Empresas</span>}
              </Button>
            </li>
            <li className="mb-3">
              <Button
                type="button"
                title="Equipamentos"
                variant={"ghost"}
                onClick={() => handleNavigation("/equipamento")}
                className="flex items-center"
              >
                <Axe className="w-5 h-5" />
                {!collapsed && <span className="ml-2">Equipamentos</span>}
              </Button>
            </li>
            <li className="mb-3">
              <Button
                type="button"
                title="Usuários"
                variant={"ghost"}
                onClick={() => handleNavigation("/usuario")}
                className="flex items-center"
              >
                <User className="w-5 h-5" />
                {!collapsed && <span className="ml-2">Usuários</span>}
              </Button>
            </li>
          </ul>
        </div>
        <div className="flex flex-col items-start mt-4">
          <Button
            type="button"
            title="Logout"
            variant={"ghost"}
            onClick={handleLogout}
            className="flex items-left my-2"
          >
            <LogOut className="w-5 h-5" />
            {!collapsed && <span className="ml-2">Encerrar sessão</span>}
          </Button>
          <div
            className={`flex items-center rounded-sm ${
              !collapsed ? "bg-[#F55139]/70 p-3 mt-4 h-20" : ""
            }`}
          >
            <Avatar className="w-11 h-11 mr-2" title={username || "Usuário"}>
              <AvatarImage src="/avatar.jpg" alt={username || "Usuário"} />
              <AvatarFallback>{username}</AvatarFallback>
            </Avatar>
            {!collapsed && username && (
              <div>
                <p className="text-white ml-2 transition-all duration-800">
                  {username}
                </p>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
