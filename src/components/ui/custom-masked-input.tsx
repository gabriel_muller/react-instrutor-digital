import React from "react";
import { UseFormRegister, Control } from "react-hook-form";
// @ts-expect-error
import InputMask from "react-input-mask";
import {
  FormField,
  FormItem,
  FormLabel,
  FormControl,
  FormMessage,
} from "./form";
import { Input } from "./input";

interface FormValues {
  cnpj: string;
  socialReason: string;
  fantasyName: string;
  zipcode?: string;
  address?: string;
  addressNumber?: string;
  addressComp?: string;
  district?: string;
  city?: string;
  state?: string;
  phone?: string;
  phoneSecondary?: string;
}

interface CustomMaskedInputProps {
  control: Control<FormValues>;
  name: keyof FormValues;
  label: string;
  mask: string;
  placeholder: string;
  register: UseFormRegister<FormValues>;
  required?: boolean;
  className?: string;
}

export const CustomMaskedInput: React.FC<CustomMaskedInputProps> = ({
  control,
  name,
  label,
  mask,
  placeholder,
  register,
  required,
  className,
}) => (
  <FormField
    control={control}
    name={name}
    render={({ field }) => (
      <FormItem className={`flex flex-col ${className}`}>
        <FormLabel>
          {label}
          {required && <b className="text-red-600 mx-1"> *</b>}
        </FormLabel>
        <FormControl>
          <InputMask mask={mask} {...field} {...register(name)}>
            {(inputProps: any) => (
              <Input placeholder={placeholder} {...inputProps} />
            )}
          </InputMask>
        </FormControl>
        <FormMessage className="mt-4" />
      </FormItem>
    )}
  />
);
