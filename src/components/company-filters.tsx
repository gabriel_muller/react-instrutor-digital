import { Card, CardContent, CardHeader } from "./ui/card";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "./ui/select";
import { Input } from "./ui/input";
import { Button } from "./ui/button";
import { Filter, FilterX } from "lucide-react";

const FilterComponent = ({
  filters,
  setFilters,
  applyFilters,
  clearFilters,
}: any) => {
  return (
    <Card className="mb-0 p-0">
      <CardHeader className="p-4">
        <h1 className="text-white text-lg">Filtros</h1>
      </CardHeader>
      <CardContent className="p-4">
        <div className="grid grid-cols-1 md:grid-cols-4 gap-4">
          <Select
            value={filters.type}
            onValueChange={(value) => setFilters({ ...filters, type: value })}
          >
            <SelectTrigger className="text-slate-400">
              <SelectValue placeholder="Tipo" />
            </SelectTrigger>
            <SelectContent>
              <SelectItem value="head">Matriz</SelectItem>
              <SelectItem value="branch">Filial</SelectItem>
            </SelectContent>
          </Select>

          <Input
            placeholder="CNPJ"
            value={filters.cnpj}
            onChange={(e) => setFilters({ ...filters, cnpj: e.target.value })}
            className="w-full"
          />

          <Input
            placeholder="Razão Social"
            value={filters.socialReason}
            onChange={(e: any) =>
              setFilters({ ...filters, socialReason: e.target.value })
            }
            className="w-full"
          />

          <div className="flex justify-end space-x-2">
            <Button
              title="Aplicar Filtros"
              className="bg-indigo-600 hover:bg-indigo-600/75 text-white"
              onClick={applyFilters}
            >
              <Filter className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base">Aplicar Filtros</h1>
            </Button>
            <Button
              title="Limpar Filtros"
              className="bg-gray-600 hover:bg-gray-600/75 text-white"
              onClick={clearFilters}
            >
              <FilterX className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base">Limpar Filtros</h1>
            </Button>
          </div>
        </div>
      </CardContent>
    </Card>
  );
};

export default FilterComponent;
