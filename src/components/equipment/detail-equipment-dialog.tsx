import { useQuery } from "@tanstack/react-query";
import { formatDateToDDMMYYYY } from "@/shared/functions/format-date";
import axiosInstance from "@/api/axios";
import {
  DialogContent,
  DialogHeader,
  DialogTitle,
  Label,
  Separator,
  Input,
} from "../ui";

type FindEquipmentByIdProps = {
  identifier: string;
  serialNumber: string;
  batch: string;
  manufacturingDate: string;
};

type DetailEquipmentDialogProps = {
  equipmentId: string;
};

export function DetailEquipmentDialog({
  equipmentId,
}: DetailEquipmentDialogProps) {
  const findEquipmentById = async (id: string) => {
    const response = await new Promise((resolve) => {
      setTimeout(async () => {
        const res = await axiosInstance.get<FindEquipmentByIdProps>(
          `/equipament/${id}`
        );
        resolve(res);
      }, 1000);
    });
    return (response as any).data;
  };

  const { data: equipment } = useQuery({
    queryKey: ["find-equipment-by-id", equipmentId],
    queryFn: () => findEquipmentById(equipmentId as string),
    enabled: !!equipmentId,
    initialData: {},
  });

  return (
    <DialogContent className="w-screen max-w-2xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Detalhes do equipamento
        </DialogTitle>
      </DialogHeader>
      <div className="grid grid-cols-12 items-center text-left gap-6 mb-4">
        <Separator className="col-span-12" />
        <div className="col-span-6 flex flex-col">
          <Label htmlFor="name" className="my-2  text-base">
            Nome
          </Label>

          <Input
            id="name"
            value={equipment?.identifier ? equipment.identifier : ""}
            disabled
          />
        </div>
        <div className="col-span-6 flex flex-col">
          <Label htmlFor="serialNumber" className="my-2  text-base">
            Número de Série
          </Label>

          <Input
            id="serialNumber"
            value={equipment?.serialNumber ? equipment.serialNumber : ""}
            disabled
          />
        </div>
      </div>
      <div className="grid grid-cols-12 items-center text-left gap-6">
        <div className="col-span-6 flex flex-col">
          <Label htmlFor="batch" className="my-2  text-base">
            Lote
          </Label>

          <Input
            id="batch"
            value={equipment?.batch ? equipment.batch : ""}
            disabled
          />
        </div>
        <div className="col-span-6 flex flex-col">
          <Label htmlFor="manufacturingDate" className="my-2  text-base">
            Data de Fabricação
          </Label>

          <Input
            id="manufacturingDate"
            value={
              equipment?.manufacturingDate
                ? formatDateToDDMMYYYY(equipment.manufacturingDate)
                : ""
            }
            disabled
          />
        </div>
      </div>
    </DialogContent>
  );
}
