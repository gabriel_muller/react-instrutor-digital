import {
  DialogHeader,
  DialogContent,
  DialogTitle,
  DialogFooter,
  DialogClose,
} from "../ui/dialog";
import { Input } from "../ui/input";
import { Button } from "../ui/button";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useToast } from "../ui/use-toast";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
import { Separator } from "../ui/separator";
import { Ban, PlusCircle } from "lucide-react";
import axiosInstance from "@/api/axios";

const createEquipmentSchema = z.object({
  name: z.string().refine(
    (value) => {
      return !!value.trim();
    },
    {
      message: "Nome é obrigatório",
    }
  ),
  serialNumber: z.string().refine(
    (value) => {
      return !!value.trim();
    },
    {
      message: "Número de série é obrigatório",
    }
  ),
  batch: z.string().refine(
    (value) => {
      return !!value.trim();
    },
    {
      message: "Lote é obrigatório",
    }
  ),
  manufacturingDate: z.string().refine(
    (value) => {
      return !!value.trim();
    },
    {
      message: "Data de fabricação é obrigatório",
    }
  ),
});

type CreateEquipmentProps = {
  identifier: string;
  serialNumber: string;
  batch: string;
  manufacturingDate: string;
};

type CreateEquipmentSchema = z.infer<typeof createEquipmentSchema>;

export function CreateEquipmentDialog() {
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const form = useForm<CreateEquipmentSchema>({
    resolver: zodResolver(createEquipmentSchema),
  });

  const createEquipment = async (data: CreateEquipmentProps) => {
    const response = await axiosInstance.post<CreateEquipmentProps>(
      "/equipament",
      data
    );
    return response.data;
  };

  const { mutateAsync: createEquipmentFn } = useMutation({
    mutationFn: createEquipment,
    onSuccess(_, variables) {
      queryClient.setQueryData(["list-equipments"], (data: any) => {
        return [
          ...(data as any),
          {
            identifier: variables.identifier,
            serialNumber: variables.serialNumber,
            batch: variables.batch,
            manufacturingDate: variables.manufacturingDate,
          },
        ];
      });
    },
  });

  async function handleCreateEquipment(data: CreateEquipmentSchema) {
    try {
      await createEquipmentFn({
        identifier: data.name,
        serialNumber: data.serialNumber,
        batch: data.batch,
        manufacturingDate: data.manufacturingDate,
      });
      toast({
        title: "Equipamento adicionado com sucesso!",
        description: `O equipamento ${data.name} foi criado`,
        duration: 3000,
      });
    } catch (error) {
      toast({
        title: "Erro!",
        description: `Não foi possível adicionar o equipamento ${data.name}`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <DialogContent className="w-screen max-w-2xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Cadastrar novo equipamento
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleCreateEquipment)}
          className="space-y-4"
        >
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Nome<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome"
                      {...field}
                      {...form.register("name")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="serialNumber"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Número de Série<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o número de série"
                      {...field}
                      {...form.register("serialNumber")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <FormField
              control={form.control}
              name="batch"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Lote<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o lote"
                      {...field}
                      {...form.register("batch")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="manufacturingDate"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Data de Fabricação<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      type="date"
                      className="text-slate-400"
                      {...field}
                      {...form.register("manufacturingDate")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <Separator className="col-span-12 mt-6" />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button
                type="button"
                className="text-white antialiased bg-slate-600 hover:bg-slate-600/75"
              >
                <Ban className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base ">Cancelar</h1>
              </Button>
            </DialogClose>
            <Button
              type="submit"
              className="text-white antialiased bg-green-700 hover:bg-green-700/75"
            >
              <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base">Adicionar</h1>
            </Button>
          </DialogFooter>
        </form>
      </Form>
    </DialogContent>
  );
}
