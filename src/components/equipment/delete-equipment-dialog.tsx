import { Ban, Trash2 } from "lucide-react";
import {
  AlertDialogHeader,
  AlertDialogFooter,
  AlertDialogContent,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogDescription,
  AlertDialogTitle,
} from "../ui/alert-dialog";
import { useToast } from "@/components/ui/use-toast";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import axiosInstance from "@/api/axios";

type DeleteEquipmentDialogProps = {
  equipmentId: string | null;
};

export function DeleteEquipmentDialog({
  equipmentId,
}: DeleteEquipmentDialogProps) {
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const deleteEquipment = async () => {
    await axiosInstance.delete(`/equipament/${equipmentId}`);
  };

  const { mutateAsync: deleteEquipmentFn } = useMutation({
    mutationKey: ["delete-equipment", equipmentId],
    mutationFn: deleteEquipment,
    onSuccess: async () => {
      queryClient.setQueryData(["list-equipments"], (oldData: any) => {
        if (!oldData) return [];
        return oldData.filter((equipment: any) => equipment.id !== equipmentId);
      });
    },
  });

  async function handleDeleteEquipment() {
    try {
      await deleteEquipmentFn();
      toast({
        title: "Equipamento deletado com sucesso!",
        description: "A lista de equipamentos foi atualizada",
        duration: 3000,
        className: "bg-green-700 border-none antialiasing",
      });
    } catch (error) {
      console.error(error);
      toast({
        title: "Erro!",
        description: "Não foi possível deletar o equipamento",
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <AlertDialogContent className="w-screen max-w-xl">
      <AlertDialogHeader>
        <AlertDialogTitle className="mb-2">
          Você tem certeza que deseja remover este equipamento?
        </AlertDialogTitle>
        <AlertDialogDescription>
          Essa ação não pode ser desfeita. Isso irá excluir permanentemente o
          equipamento de nossos servidores.
        </AlertDialogDescription>
      </AlertDialogHeader>
      <AlertDialogFooter className="mt-6">
        <AlertDialogCancel className="text-white bg-slate-600 hover:bg-slate-600/75">
          <Ban className="w-4 h-4 mr-2" />
          <h1 className="text-base">Cancelar</h1>
        </AlertDialogCancel>
        <AlertDialogAction
          onClick={handleDeleteEquipment}
          className="text-white bg-red-600 hover:bg-red-600/75"
        >
          <Trash2 className="w-4 h-4 mr-2" />
          <h1 className="text-base">Remover</h1>
        </AlertDialogAction>
      </AlertDialogFooter>
    </AlertDialogContent>
  );
}
