import { Ban, Pencil } from "lucide-react";
import {
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "../ui/dialog";
import { Input } from "../ui/input";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useToast } from "../ui/use-toast";
import { Separator } from "../ui/separator";
import { Button } from "../ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
// @ts-expect-error
import InputMask from "react-input-mask";
import {
  formatDateToDDMMYYYY,
  formatDateToISOString,
} from "@/shared/functions/format-date";
import axiosInstance from "@/api/axios";

const updateEquipmentSchema = z.object({
  identifier: z.string(),
  serialNumber: z.string(),
  batch: z.string(),
  manufacturingDate: z.string(),
});

type UpdateEquipmentProps = {
  identifier: string;
  serialNumber: string;
  batch: string;
  manufacturingDate: string;
};

type FindEquipmentByIdProps = {
  identifier: string;
  serialNumber: string;
  batch: string;
  manufacturingDate: string;
};

type UpdateEquipmentDialogProps = {
  equipmentId: string;
};

type UpdateEquipmentSchema = z.infer<typeof updateEquipmentSchema>;

export function UpdateEquipmentDialog({
  equipmentId,
}: UpdateEquipmentDialogProps) {
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const updateEquipment = async (data: UpdateEquipmentProps) => {
    const response = await axiosInstance.patch<UpdateEquipmentProps>(
      `/equipament/${equipmentId}`,
      data
    );
    return response.data;
  };

  const findEquipmentById = async (id: string | null) => {
    if (!id) return {};
    const response = await axiosInstance.get<FindEquipmentByIdProps>(
      `/equipament/${id}`
    );
    console.log(response.data);
    const data = response.data;

    form.setValue("identifier", data.identifier);
    form.setValue("serialNumber", data.serialNumber);
    form.setValue("batch", data.batch);
    form.setValue(
      "manufacturingDate",
      formatDateToDDMMYYYY(data.manufacturingDate)
    );

    return data;
  };

  const { mutateAsync: updateEquipmentFn } = useMutation({
    mutationKey: ["update-equipment", equipmentId],
    mutationFn: updateEquipment,
    onSuccess(_, variables) {
      queryClient.setQueryData(["list-equipments"], (data: any) => {
        if (!data) return data;
        const updatedData = data.map((equipment: any) => {
          if (equipment.id === equipmentId) {
            return {
              ...equipment,
              identifier: variables.identifier,
              serialNumber: variables.serialNumber,
              batch: variables.batch,
              manufacturingDate: variables.manufacturingDate,
            };
          }
          return equipment;
        });
        return updatedData;
      });
    },
  });

  useQuery({
    queryKey: ["find-equipment-by-id", equipmentId],
    queryFn: () => findEquipmentById(equipmentId),
    enabled: !!equipmentId,
    initialData: {},
  });

  const form = useForm<UpdateEquipmentSchema>({
    resolver: zodResolver(updateEquipmentSchema),
  });

  async function handleUpdateEquipment(data: UpdateEquipmentSchema) {
    console.log(data.manufacturingDate);
    try {
      await updateEquipmentFn({
        identifier: data.identifier,
        serialNumber: data.serialNumber,
        batch: data.batch,
        manufacturingDate: formatDateToISOString(data.manufacturingDate),
      });
      toast({
        title: "Equipamento editado com sucesso!",
        description: `A lista de equipamentos foi atualizada`,
        duration: 3000,
      });
    } catch (error) {
      console.error(error);
      toast({
        title: "Erro!",
        description: `Não foi possível editar o equipamento`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <DialogContent className="w-screen max-w-2xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Editar equipamento
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleUpdateEquipment)}
          className="space-y-4"
        >
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="identifier"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Nome<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome"
                      {...field}
                      {...form.register("identifier")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="serialNumber"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Número de Série<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o número de série"
                      {...field}
                      {...form.register("serialNumber")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <FormField
              control={form.control}
              name="batch"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Lote<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o lote"
                      {...field}
                      {...form.register("batch")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="manufacturingDate"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Data de Fabricação<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <InputMask
                      mask="99-99-9999"
                      placeholder="DD-MM-YYYY"
                      {...field}
                      value={field.value}
                      onChange={field.onChange}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <Separator className="col-span-12 mt-6" />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button
                type="button"
                className="text-white bg-slate-600 hover:bg-slate-600/75"
              >
                <Ban className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base ">Cancelar</h1>
              </Button>
            </DialogClose>
            <Button
              type="submit"
              className="text-white bg-amber-600 hover:bg-amber-600/75"
            >
              <Pencil className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base ">Atualizar</h1>
            </Button>
          </DialogFooter>
        </form>
      </Form>
    </DialogContent>
  );
}
