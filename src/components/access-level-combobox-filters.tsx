import * as React from "react";
import { Check, ChevronsUpDown } from "lucide-react";

import { cn } from "@/lib/utils";
import { Button } from "@/components/ui/button";
import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from "@/components/ui/command";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { GlobalAccessLevel } from "@/shared/enums/access-level-enum";
import { acessLevels } from "@/shared/enums/access-level-key-value";

type ComboboxDemoProps = {
  value: any;
  onSelect: (value: GlobalAccessLevel) => void;
};

export function AccessLevelComboboxFilters({
  value,
  onSelect,
}: ComboboxDemoProps) {
  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState<string | undefined>(
    value
  );

  React.useEffect(() => {
    setSelectedValue(value);
  }, [value]);

  return (
    <Popover open={open} onOpenChange={setOpen}>
      <PopoverTrigger asChild>
        <Button
          variant="outline"
          role="combobox"
          aria-expanded={open}
          className="justify-between font-thin text-slate-100"
        >
          {selectedValue ? (
            acessLevels.find(
              (acessLevel) =>
                selectedValue && acessLevel.value === selectedValue
            )?.label
          ) : (
            <h1 className="text-slate-400">Selecione o nível de acesso</h1>
          )}
          <ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
        </Button>
      </PopoverTrigger>
      <PopoverContent className="h-[120px] p-0">
        <Command>
          <CommandInput placeholder="Busque o nível de acesso" />
          <CommandEmpty>0 níveis de acesso encontrados</CommandEmpty>
          <CommandList>
            <CommandGroup>
              {acessLevels.map((acessLevel: any) => (
                <CommandItem
                  key={acessLevel.value}
                  value={acessLevel.value}
                  onSelect={(currentValue) => {
                    setSelectedValue(
                      currentValue === selectedValue ? undefined : currentValue
                    );
                    setOpen(false);
                    onSelect(acessLevel.value);
                  }}
                >
                  <Check
                    className={cn(
                      "mr-2 h-4 w-4",
                      selectedValue === acessLevel.value
                        ? "opacity-100"
                        : "opacity-0"
                    )}
                  />
                  {acessLevel.label}
                </CommandItem>
              ))}
            </CommandGroup>
          </CommandList>
        </Command>
      </PopoverContent>
    </Popover>
  );
}
