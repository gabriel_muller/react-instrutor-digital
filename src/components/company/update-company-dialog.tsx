import { Ban, Pencil } from "lucide-react";
import {
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "../ui/dialog";
import { Input } from "../ui/input";
import { z } from "zod";
import React from "react";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useToast } from "../ui/use-toast";
import { Separator } from "../ui/separator";
import { Button } from "../ui/button";
import { StateComboboxFilters } from "../state-combobox-filters";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
// @ts-expect-error
import InputMask from "react-input-mask";
import { UF } from "@/shared/enums/state-enum";
import { getCityIdByName } from "@/shared/requests/get-city-id-by-name";
import axiosInstance from "@/api/axios";

const updateCompanySchema = z.object({
  cnpj: z.string().optional(),
  socialReason: z.string().optional(),
  fantasyName: z.string().optional(),
  zipcode: z.string().optional(),
  address: z.string().optional(),
  addressNumber: z.string().optional(),
  addressComp: z.string().nullable().optional(),
  addressCity: z.string().optional(),
  addressUf: z.string().optional(),
  email: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return value === null || value === "" || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "Campo deve ser um e-mail válido ou estar vazio",
      }
    )
    .optional(),
  emailSecondary: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .nullable()
    .refine(
      (value) => {
        return value === null || value === "" || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "Campo deve ser um e-mail válido ou estar vazio",
      }
    )
    .optional(),
  phone: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return value === null || /^\d*$/.test(value);
      },
      {
        message: "Campo deve ter apenas números ou estar vazio",
      }
    )
    .optional(),
  phoneSecondary: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .nullable()
    .refine(
      (value) => {
        return value === null || /^\d*$/.test(value);
      },
      {
        message: "Campo deve ter apenas números ou estar vazio",
      }
    )
    .optional(),
  headOffice: z.boolean().optional(),
});

type UpdateCompanyProps = {
  socialReason?: string;
  fantasyName?: string;
  zipcode?: string;
  address?: string;
  addressNumber?: string;
  addressComp?: string | null;
  addressCity?: string;
  addressCityId?: number;
  addressUf?: string;
  email?: string;
  emailSecondary?: string | null;
  phone?: string;
  phoneSecondary?: string | null;
};

type FindCompanyByIdProps = {
  cnpj: string;
  socialReason: string;
  fantasyName: string;
  address: string;
  addressNumber: string;
  addressComp: string;
  zipcode: string;
  city: {
    name: string;
    state: UF;
  };
  email: string;
  emailSecondary: string;
  phone: string;
  phoneSecondary: string;
  headOffice: boolean;
};

type UpdateCompanyDialogProps = {
  companyId: string;
  companyName?: string;
};

type UpdateCompanySchema = z.infer<typeof updateCompanySchema>;

export function UpdateCompanyDialog({ companyId }: UpdateCompanyDialogProps) {
  const [_selectedAddressState, setSelectedAddressState] = React.useState("");

  const queryClient = useQueryClient();
  const { toast } = useToast();

  const updateCompany = async (data: UpdateCompanyProps) => {
    const response = await axiosInstance.patch<UpdateCompanyProps>(
      `/company/${companyId}`,
      data
    );
    return response.data;
  };

  const findCompanyById = async (id: string | null) => {
    if (!id) return {};
    const response = await axiosInstance.get<FindCompanyByIdProps>(
      `/company/${id}`
    );
    const data = response.data;

    form.setValue("cnpj", data.cnpj);
    form.setValue("socialReason", data.socialReason);
    form.setValue("fantasyName", data.fantasyName);
    form.setValue("address", data.address);
    form.setValue("addressNumber", data.addressNumber);
    form.setValue("addressComp", data.addressComp || "");
    form.setValue("addressCity", data.city.name);
    form.setValue("addressUf", data.city.state);
    form.setValue("zipcode", data.zipcode);
    form.setValue("email", data.email);
    form.setValue("emailSecondary", data.emailSecondary || "");
    form.setValue("phone", data.phone);
    form.setValue("phoneSecondary", data.phoneSecondary || "");

    return data;
  };

  const { mutateAsync: updateCompanyFn } = useMutation({
    mutationKey: ["update-company", companyId],
    mutationFn: updateCompany,
    onSuccess(_, variables) {
      queryClient.setQueryData(["list-companies"], (data: any) => {
        if (!data) return data; // Retorna os dados sem modificações se forem indefinidos
        const updatedData = data.map((company: any) => {
          if (company.id === companyId) {
            return {
              ...company,
              socialReason: variables.socialReason,
              fantasyName: variables.fantasyName,
              zipcode: variables.zipcode,
              address: variables.address,
              addressNumber: variables.addressNumber,
              addressComp: variables.addressComp,
              addressCity: variables.addressCity,
              addressUf: variables.addressUf,
              email: variables.email,
              emailSecondary: variables.emailSecondary,
              phone: variables.phone,
              phoneSecondary: variables.phoneSecondary,
            };
          }
          return company;
        });
        return updatedData;
      });
    },
  });

  useQuery({
    queryKey: ["find-company-by-id", companyId],
    queryFn: () => findCompanyById(companyId),
    enabled: !!companyId,
    initialData: {},
  });

  const form = useForm<UpdateCompanySchema>({
    resolver: zodResolver(updateCompanySchema),
  });

  async function handleUpdateCompany(data: UpdateCompanySchema) {
    const addressCityId = await getCityIdByName(
      data.addressCity ? data.addressCity : ""
    );
    try {
      await updateCompanyFn({
        socialReason: data.socialReason,
        fantasyName: data.fantasyName,
        zipcode: data.zipcode,
        address: data.address,
        addressNumber: data.addressNumber,
        addressComp: data.addressComp ? data.addressComp : null,
        addressUf: data.addressUf as UF,
        addressCityId: addressCityId,
        email: data.email,
        emailSecondary: data.emailSecondary ? data.emailSecondary : null,
        phone: data.phone,
        phoneSecondary: data.phoneSecondary ? data.phoneSecondary : null,
      });
      toast({
        title: "Empresa editada com sucesso!",
        description: `A empresa foi atualizada`,
        duration: 3000,
      });
    } catch (error) {
      console.error(error);
      toast({
        title: "Erro!",
        description: `Não foi possível editar a empresa`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <DialogContent className="w-screen max-w-6xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Editar empresa
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleUpdateCompany)}
          className="space-y-4"
        >
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="cnpj"
              disabled={true}
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>CNPJ</FormLabel>
                  <FormControl>
                    <InputMask mask="99.999.999/9999-99" {...field}>
                      {(inputProps: any) => (
                        <Input {...inputProps} disabled={field.disabled} />
                      )}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="socialReason"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>Razão Social</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a razão social"
                      {...field}
                      {...form.register("socialReason")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="fantasyName"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>Nome Fantasia</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome fantasia"
                      {...field}
                      {...form.register("fantasyName")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Logradouro
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="zipcode"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>CEP</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="99999-999"
                      placeholder="Digite o CEP"
                      {...field}
                      {...form.register("zipcode")}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="address"
              render={({ field }) => (
                <FormItem className="col-span-5 flex flex-col">
                  <FormLabel>Endereço</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o endereço"
                      {...field}
                      {...form.register("address")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressNumber"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>Número</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a número"
                      {...field}
                      {...form.register("addressNumber")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressComp"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Complemento</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o complemento"
                      {...field}
                      value={field.value ?? ""}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressUf"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>Estado</FormLabel>
                  <FormControl>
                    <StateComboboxFilters
                      onSelect={(selectedValue) => {
                        setSelectedAddressState(selectedValue);
                        form.setValue("addressUf", selectedValue);
                      }}
                      {...field}
                      {...form.register("addressUf")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressCity"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Cidade</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a cidade"
                      {...field}
                      {...form.register("addressCity")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Contato
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>E-mail Principal</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o e-mail"
                      {...field}
                      {...form.register("email")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="emailSecondary"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>E-mail Secundário</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o e-mail secundário"
                      {...field}
                      value={field.value ?? ""}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="phone"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Telefone Principal</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone"
                      {...field}
                      {...form.register("phone")}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="phoneSecondary"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Telefone Secundário</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone secundário"
                      {...field}
                      {...form.register("phoneSecondary")}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <Separator className="col-span-12 mt-6" />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button
                type="button"
                className="text-white bg-slate-600 hover:bg-slate-600/75"
              >
                <Ban className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base ">Cancelar</h1>
              </Button>
            </DialogClose>
            <Button
              type="submit"
              className="text-white bg-amber-600 hover:bg-amber-600/75"
            >
              <Pencil className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base ">Atualizar</h1>
            </Button>
          </DialogFooter>
        </form>
      </Form>
    </DialogContent>
  );
}
