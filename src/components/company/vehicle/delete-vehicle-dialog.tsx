import axiosInstance from "@/api/axios";
import {
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  useToast,
} from "@/components/ui";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { Ban, Trash2 } from "lucide-react";

type DeleteVehicleDialogProps = {
  companyId?: string | null;
  vehicleId?: string | null;
};

export function DeleteVehicleDialog({
  companyId,
  vehicleId,
}: DeleteVehicleDialogProps) {
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const deleteVehicle = async () => {
    if (companyId && vehicleId) {
      await axiosInstance.delete(`/company/${companyId}/vehicle/${vehicleId}`);
    } else {
      throw new Error("companyId or vehicleId is missing");
    }
  };

  const { mutateAsync: deleteVehicleFn } = useMutation({
    mutationKey: ["delete-vehicle", vehicleId],
    mutationFn: deleteVehicle,
    onSuccess: () => {
      queryClient.setQueryData(["list-vehicles"], (oldData: any) => {
        return oldData
          ? oldData.filter((vehicle: any) => vehicle.id !== vehicleId)
          : [];
      });
      toast({
        title: "Veículo deletado com sucesso!",
        description: "A lista de veículos foi atualizada.",
        duration: 3000,
        className: "bg-green-700 border-none antialiasing",
      });
    },
    onError: () => {
      toast({
        title: "Erro!",
        description: "Não foi possível deletar o veículo.",
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    },
  });

  return (
    <AlertDialogContent>
      <AlertDialogHeader>
        <AlertDialogTitle className="mb-2">
          Você tem certeza que deseja remover este veículo?
        </AlertDialogTitle>
        <AlertDialogDescription>
          Essa ação não pode ser desfeita. Isso irá excluir permanentemente de
          nossos servidores.
        </AlertDialogDescription>
      </AlertDialogHeader>
      <AlertDialogFooter className="mt-6">
        <AlertDialogCancel className="text-white bg-slate-600 hover:bg-slate-600/75">
          <Ban className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base">Cancelar</h1>
        </AlertDialogCancel>
        <AlertDialogAction
          onClick={() => deleteVehicleFn()}
          className="text-white bg-red-600 hover:bg-red-600/75"
        >
          <Trash2 className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base">Remover</h1>
        </AlertDialogAction>
      </AlertDialogFooter>
    </AlertDialogContent>
  );
}
