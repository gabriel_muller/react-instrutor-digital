import axiosInstance from "@/api/axios";
import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Separator,
  useToast,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/components/ui";
import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { Ban, CircleMinus, Pencil, Truck } from "lucide-react";
import { useFieldArray, useForm } from "react-hook-form";
import { z } from "zod";

const updateVehicleSchema = z.object({
  axle: z.number(),
  segment: z.string().nullable().optional(),
  group: z.string().nullable().optional(),
  capacity: z.string().nullable().optional(),
  ownerName: z.string().nullable().optional(),
  ownerCpfCnpj: z.string().nullable().optional(),
  licensePlate: z.string(),
  chassi: z.string(),
  brand: z.string(),
  model: z.string(),
  manufactureYear: z.number(),
  fuelType: z.string(),
  rntrc: z.string().nullable().optional(),
  trailers: z
    .array(
      z.object({
        id: z.string().optional(),
        licensePlate: z.string(),
        chassi: z.string(),
        brand: z.string(),
        model: z.string(),
        manufactureYear: z.number(),
        type: z.string(),
      })
    )
    .optional(),
});

type UpdateVehicleProps = z.infer<typeof updateVehicleSchema>;

interface UpdateVehicleDialogProps {
  companyId?: string | null;
  vehicleId?: string | null;
}

const findVehicleById = async (
  companyId: string | null,
  vehicleId: string | null,
  form: any
) => {
  if (!companyId || !vehicleId) return {};
  const response = await axiosInstance.get<UpdateVehicleProps>(
    `/company/${companyId}/vehicle/${vehicleId}`
  );
  const data = response.data;

  form.setValue("axle", Number(data.axle));
  form.setValue("segment", data.segment || "");
  form.setValue("group", data.group || "");
  form.setValue("capacity", data.capacity || "");
  form.setValue("ownerName", data.ownerName || "");
  form.setValue("ownerCpfCnpj", data.ownerCpfCnpj || "");
  form.setValue("licensePlate", data.licensePlate);
  form.setValue("chassi", data.chassi);
  form.setValue("brand", data.brand);
  form.setValue("model", data.model);
  form.setValue("manufactureYear", Number(data.manufactureYear));
  form.setValue("fuelType", data.fuelType);
  form.setValue("rntrc", data.rntrc || "");

  return data;
};

const findTrailersById = async (
  companyId: string | null,
  vehicleId: string | null
) => {
  if (!companyId || !vehicleId) return [];
  const response = await axiosInstance.get<any>(
    `/company/${companyId}/vehicle/${vehicleId}/vehicleCart`
  );
  const data = response.data;

  return data;
};

export function UpdateVehicleDialog({
  companyId,
  vehicleId,
}: UpdateVehicleDialogProps) {
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const form = useForm<UpdateVehicleProps>({
    resolver: zodResolver(updateVehicleSchema),
  });

  const { fields, append, remove } = useFieldArray({
    control: form.control,
    name: "trailers",
  });

  const updateVehicle = async (data: UpdateVehicleProps) => {
    const response = await axiosInstance.patch(
      `/company/${companyId}/vehicle/${vehicleId}`,
      {
        axle: data.axle,
        segment: data.segment ? data.segment : null,
        group: data.group ? data.group : null,
        capacity: data.capacity ? data.capacity : null,
        ownerName: data.ownerName ? data.ownerName : null,
        ownerCpfCnpj: data.ownerCpfCnpj ? data.ownerCpfCnpj : null,
        licensePlate: data.licensePlate,
        chassi: data.chassi,
        brand: data.brand,
        model: data.model,
        manufactureYear: data.manufactureYear,
        fuelType: data.fuelType,
        rntrc: data.rntrc ? data.rntrc : null,
      }
    );

    if (data?.trailers && data?.trailers.length) {
      for (const trailer of data.trailers) {
        if (trailer.id) {
          await axiosInstance.patch(
            `/company/${companyId}/vehicle/${response.data.id}/vehicleCart/${trailer.id}`,
            {
              licensePlate: trailer.licensePlate,
              chassi: trailer.chassi,
              type: trailer.type,
              brand: trailer.brand,
              model: trailer.model,
              manufactureYear: trailer.manufactureYear,
            }
          );
        } else {
          await axiosInstance.post(
            `/company/${companyId}/vehicle/${response.data.id}/vehicleCart`,
            {
              licensePlate: trailer.licensePlate,
              chassi: trailer.chassi,
              type: trailer.type,
              brand: trailer.brand,
              model: trailer.model,
              manufactureYear: trailer.manufactureYear,
            }
          );
        }
      }
    }
    return response.data;
  };

  const { mutateAsync: updateVehicleFn } = useMutation({
    mutationFn: updateVehicle,
    onSuccess: (_, variables) => {
      queryClient.setQueryData(["find-vehicle-by-id"], (data: any) => {
        return Array.isArray(data) ? [...data, variables] : [variables];
      });
    },
  });

  useQuery({
    queryKey: ["find-vehicle-cart-by-vehicle-id", companyId, vehicleId],
    queryFn: async () => {
      const vehicleData = await findVehicleById(
        companyId as string,
        vehicleId as string,
        form
      );
      const trailers = await findTrailersById(
        companyId as string,
        vehicleId as string
      );
      trailers.forEach((trailer: any) => append(trailer));
      return { vehicleData, trailers };
    },
    enabled: !!companyId && !!vehicleId,
  });

  async function handleUpdateVehicle(data: UpdateVehicleProps) {
    try {
      await updateVehicleFn(data);
      toast({
        title: "Veículo editado com sucesso!",
        description: `A lista de veículos foi atualizada.`,
        duration: 2000,
      });
    } catch (error) {
      console.error(error);
      toast({
        title: "Erro!",
        description: `Não foi possível editar o veículo.`,
        duration: 2000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <DialogContent className="w-screen max-w-6xl max-h-screen overflow-y-auto">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Editar veículo
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleUpdateVehicle)}
          className="space-y-4"
        >
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="axle"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>
                    Eixos<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      type="number"
                      placeholder="Digite a quantidade de eixos"
                      {...field}
                      {...form.register("axle", { valueAsNumber: true })}
                    />
                  </FormControl>
                  <FormMessage className="mt-4" />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="segment"
              render={() => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Segmento</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o segmento"
                      {...form.register("segment")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="group"
              render={() => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Grupo</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o grupo"
                      {...form.register("group")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="capacity"
              render={() => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Capacidade</FormLabel>

                  <FormControl>
                    <Input
                      placeholder="Digite a capacidade"
                      {...form.register("capacity")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>

          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Proprietário
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="ownerName"
              render={() => (
                <FormItem className="col-span-5 flex flex-col">
                  <FormLabel>Nome do Proprietário</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome do proprietário"
                      {...form.register("ownerName")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="ownerCpfCnpj"
              render={() => (
                <FormItem className="col-span-5 flex flex-col">
                  <FormLabel>
                    Documento do Proprietário <i>(CPF ou CNPJ)</i>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o documento do proprietário"
                      {...form.register("ownerCpfCnpj")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold mt-6">
              Cavalo
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="licensePlate"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>
                    Placa<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a placa"
                      {...field}
                      {...form.register("licensePlate")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="chassi"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>
                    Chassi<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o número do chassi"
                      {...field}
                      {...form.register("chassi")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="brand"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>
                    Marca<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a marca"
                      {...field}
                      {...form.register("brand")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="model"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>
                    Modelo<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o modelo"
                      {...field}
                      {...form.register("model")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="manufactureYear"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>
                    Ano de Fabricação<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      type="number"
                      maxLength={4}
                      placeholder="Digite o ano de fabricação"
                      {...field}
                      {...form.register("manufactureYear")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="fuelType"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>
                    Tipo de Combustível<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o tipo de combustível"
                      {...field}
                      {...form.register("fuelType")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="rntrc"
              render={() => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>RNTRC</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o rntrc"
                      {...form.register("rntrc")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>

          {fields.map((field, index) => (
            <div
              key={field.id}
              className="grid grid-cols-12 items-center text-left gap-6 relative"
            >
              <DialogDescription className="col-span-12 flex justify-between items-center text-lg font-semibold mt-6">
                Carreta {index + 1}
                <button
                  type="button"
                  title="Remover Carreta"
                  onClick={() => remove(index)}
                  className="ml-4"
                >
                  <CircleMinus className="w-6 h-6 text-red-600 hover:text-red-600/75" />
                </button>
              </DialogDescription>
              <Separator className="col-span-12" />
              <FormField
                control={form.control}
                name={`trailers.${index}.licensePlate`}
                render={({ field }) => (
                  <FormItem className="col-span-4 flex flex-col">
                    <FormLabel>
                      Placa<b className="text-red-600 mx-1"> *</b>
                    </FormLabel>
                    <FormControl>
                      <Input placeholder="Digite a placa" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name={`trailers.${index}.chassi`}
                render={({ field }) => (
                  <FormItem className="col-span-4 flex flex-col">
                    <FormLabel>
                      Chassi<b className="text-red-600 mx-1"> *</b>
                    </FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Digite o número do chassi"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name={`trailers.${index}.brand`}
                render={({ field }) => (
                  <FormItem className="col-span-4 flex flex-col">
                    <FormLabel>
                      Marca<b className="text-red-600 mx-1"> *</b>
                    </FormLabel>
                    <FormControl>
                      <Input placeholder="Digite a marca" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name={`trailers.${index}.model`}
                render={({ field }) => (
                  <FormItem className="col-span-3 flex flex-col">
                    <FormLabel>
                      Modelo<b className="text-red-600 mx-1"> *</b>
                    </FormLabel>
                    <FormControl>
                      <Input placeholder="Digite o modelo" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name={`trailers.${index}.manufactureYear`}
                render={({ field }) => (
                  <FormItem className="col-span-3 flex flex-col">
                    <FormLabel>
                      Ano de Fabricação<b className="text-red-600 mx-1"> *</b>
                    </FormLabel>
                    <FormControl>
                      <Input
                        type="number"
                        placeholder="Digite o ano de fabricação"
                        {...field}
                        {...form.register(`trailers.${index}.manufactureYear`, {
                          valueAsNumber: true,
                        })}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name={`trailers.${index}.type`}
                render={({ field }) => (
                  <FormItem className="col-span-3 flex flex-col">
                    <FormLabel>
                      Tipo de Carreta<b className="text-red-600 mx-1"> *</b>
                    </FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Digite o tipo da carreta"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
          ))}

          <Button
            type="button"
            onClick={() =>
              append({
                licensePlate: "",
                chassi: "",
                brand: "",
                model: "",
                manufactureYear: new Date().getFullYear(),
                type: "",
              })
            }
            className="text-white antialiased bg-sky-700 hover:bg-sky-700/75"
          >
            <Truck className="w-4 h-4 mr-2" />
            <h1 className="text-sm">Adicionar Carreta</h1>
          </Button>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <Separator className="col-span-12 mt-6" />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button
                type="button"
                className="text-white antialiased bg-slate-600 hover:bg-slate-600/75"
              >
                <Ban className="w-4 h-4 mr-2" />
                <h1 className="text-sm">Cancelar</h1>
              </Button>
            </DialogClose>
            <Button
              type="submit"
              className="text-white bg-amber-600 hover:bg-amber-600/75"
            >
              <Pencil className="w-4 h-4 mr-2" />
              <h1 className="text-sm ">Atualizar</h1>
            </Button>
          </DialogFooter>
        </form>
      </Form>
    </DialogContent>
  );
}
