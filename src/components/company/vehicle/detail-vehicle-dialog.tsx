import { useQuery } from "@tanstack/react-query";
import axiosInstance from "@/api/axios";
import {
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  Input,
  Label,
  Separator,
} from "@/components/ui";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";

const detailVehicleSchema = z.object({
  axle: z.number(),
  segment: z.string().nullable().optional(),
  group: z.string().nullable().optional(),
  capacity: z.string().nullable().optional(),
  ownerName: z.string().nullable().optional(),
  ownerCpfCnpj: z.string().nullable().optional(),
  licensePlate: z.string(),
  chassi: z.string(),
  brand: z.string(),
  model: z.string(),
  manufactureYear: z.number(),
  fuelType: z.string(),
  rntrc: z.string().nullable().optional(),
  trailers: z
    .array(
      z.object({
        id: z.string().optional(),
        licensePlate: z.string(),
        chassi: z.string(),
        brand: z.string(),
        model: z.string(),
        manufactureYear: z.number(),
        type: z.string(),
      })
    )
    .optional(),
});

type DetailVehicleDialogProps = {
  companyId?: string | null;
  vehicleId?: string | null;
};

type DetailVehicleSchema = z.infer<typeof detailVehicleSchema>;

const findVehicleById = async (
  companyId?: string | null,
  vehicleId?: string | null
) => {
  const response = await axiosInstance.get(
    `/company/${companyId}/vehicle/${vehicleId}`
  );
  return response.data;
};

const findTrailersById = async (
  companyId?: string | null,
  vehicleId?: string | null
) => {
  const response = await axiosInstance.get(
    `/company/${companyId}/vehicle/${vehicleId}/vehicleCart`
  );
  return response.data;
};

export function DetailVehicleDialog({
  companyId,
  vehicleId,
}: DetailVehicleDialogProps) {
  useForm<DetailVehicleSchema>({
    resolver: zodResolver(detailVehicleSchema),
  });

  const { data: vehicle } = useQuery({
    queryKey: ["find-vehicle-by-id", companyId, vehicleId],
    queryFn: () => findVehicleById(companyId, vehicleId),
  });

  const { data: trailers } = useQuery({
    queryKey: ["find-trailers-by-vehicle-id", companyId, vehicleId],
    queryFn: () => findTrailersById(companyId, vehicleId),
  });

  return (
    <DialogContent className="w-screen max-w-6xl max-h-screen overflow-y-auto">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Detalhes do veículo
        </DialogTitle>
      </DialogHeader>
      <div className="space-y-4">
        <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
          <Separator className="col-span-12" />
          <div className="col-span-3 flex flex-col">
            <Label className="my-2" htmlFor="axle">
              Eixos
            </Label>
            <Input id="axle" value={vehicle?.axle ?? ""} readOnly disabled />
          </div>
          <div className="col-span-3 flex flex-col">
            <Label className="my-2" htmlFor="segment">
              Segmento
            </Label>
            <Input
              id="segment"
              value={vehicle?.segment ?? ""}
              readOnly
              disabled
            />
          </div>
          <div className="col-span-3 flex flex-col">
            <Label htmlFor="group">Grupo</Label>
            <Input id="group" value={vehicle?.group ?? ""} readOnly disabled />
          </div>
          <div className="col-span-3 flex flex-col">
            <Label className="my-2" htmlFor="capacity">
              Capacidade
            </Label>
            <Input
              id="capacity"
              value={vehicle?.capacity ?? ""}
              readOnly
              disabled
            />
          </div>
        </div>

        <div className="grid grid-cols-12 items-center text-left gap-6">
          <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
            Proprietário
          </DialogDescription>
          <Separator className="col-span-12" />
          <div className="col-span-5 flex flex-col">
            <Label className="my-2" htmlFor="ownerName">
              Nome do Proprietário
            </Label>
            <Input
              id="ownerName"
              value={vehicle?.ownerName ?? ""}
              readOnly
              disabled
            />
          </div>
          <div className="col-span-5 flex flex-col">
            <Label className="my-2" htmlFor="ownerCpfCnpj">
              Documento do Proprietário (CPF ou CNPJ)
            </Label>
            <Input
              id="ownerCpfCnpj"
              value={vehicle?.ownerCpfCnpj ?? ""}
              readOnly
              disabled
            />
          </div>
        </div>

        <div className="grid grid-cols-12 items-center text-left gap-6">
          <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold mt-6">
            Cavalo
          </DialogDescription>
          <Separator className="col-span-12" />
          <div className="col-span-4 flex flex-col">
            <Label className="my-2" htmlFor="licensePlate">
              Placa
            </Label>
            <Input
              id="licensePlate"
              value={vehicle?.licensePlate ?? ""}
              readOnly
              disabled
            />
          </div>
          <div className="col-span-4 flex flex-col">
            <Label className="my-2" htmlFor="chassi">
              Chassi
            </Label>
            <Input
              id="chassi"
              value={vehicle?.chassi ?? ""}
              readOnly
              disabled
            />
          </div>
          <div className="col-span-4 flex flex-col">
            <Label className="my-2" htmlFor="brand">
              Marca
            </Label>
            <Input id="brand" value={vehicle?.brand ?? ""} readOnly disabled />
          </div>
          <div className="col-span-3 flex flex-col">
            <Label className="my-2" htmlFor="model">
              Modelo
            </Label>
            <Input id="model" value={vehicle?.model ?? ""} readOnly disabled />
          </div>
          <div className="col-span-3 flex flex-col">
            <Label className="my-2" htmlFor="manufactureYear">
              Ano de Fabricação
            </Label>
            <Input
              id="manufactureYear"
              type="number"
              value={vehicle?.manufactureYear ?? ""}
              readOnly
              disabled
            />
          </div>
          <div className="col-span-3 flex flex-col">
            <Label className="my-2" htmlFor="fuelType">
              Tipo de Combustível
            </Label>
            <Input
              id="fuelType"
              value={vehicle?.fuelType ?? ""}
              readOnly
              disabled
            />
          </div>
          <div className="col-span-3 flex flex-col">
            <Label className="my-2" htmlFor="rntrc">
              RNTRC
            </Label>
            <Input id="rntrc" value={vehicle?.rntrc ?? ""} readOnly disabled />
          </div>
        </div>

        {trailers?.map((trailer: any, index: number) => (
          <div
            key={index}
            className="grid grid-cols-12 items-center text-left gap-6 relative"
          >
            <DialogDescription className="col-span-12 flex justify-between items-center text-lg font-semibold mt-6">
              Carreta {index + 1}
            </DialogDescription>
            <Separator className="col-span-12" />
            <div className="col-span-4 flex flex-col">
              <Label
                className="my-2"
                htmlFor={`trailers.${index}.licensePlate`}
              >
                Placa
              </Label>
              <Input
                id={`trailers.${index}.licensePlate`}
                value={trailer.licensePlate}
                readOnly
                disabled
              />
            </div>
            <div className="col-span-4 flex flex-col">
              <Label className="my-2" htmlFor={`trailers.${index}.chassi`}>
                Chassi
              </Label>
              <Input
                id={`trailers.${index}.chassi`}
                value={trailer.chassi}
                readOnly
                disabled
              />
            </div>
            <div className="col-span-4 flex flex-col">
              <Label className="my-2" htmlFor={`trailers.${index}.brand`}>
                Marca
              </Label>
              <Input
                id={`trailers.${index}.brand`}
                value={trailer.brand}
                readOnly
                disabled
              />
            </div>
            <div className="col-span-3 flex flex-col">
              <Label className="my-2" htmlFor={`trailers.${index}.model`}>
                Modelo
              </Label>
              <Input
                id={`trailers.${index}.model`}
                value={trailer.model}
                readOnly
                disabled
              />
            </div>
            <div className="col-span-3 flex flex-col">
              <Label
                className="my-2"
                htmlFor={`trailers.${index}.manufactureYear`}
              >
                Ano de Fabricação
              </Label>
              <Input
                id={`trailers.${index}.manufactureYear`}
                type="number"
                value={trailer.manufactureYear}
                readOnly
                disabled
              />
            </div>
            <div className="col-span-3 flex flex-col">
              <Label className="my-2" htmlFor={`trailers.${index}.type`}>
                Tipo de Carreta
              </Label>
              <Input
                id={`trailers.${index}.type`}
                value={trailer.type}
                readOnly
                disabled
              />
            </div>
          </div>
        ))}
      </div>
    </DialogContent>
  );
}
