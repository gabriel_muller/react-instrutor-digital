import { Ban, Trash2 } from "lucide-react";
import {
  AlertDialogHeader,
  AlertDialogFooter,
  AlertDialogContent,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogDescription,
  AlertDialogTitle,
} from "../../ui/alert-dialog";
import { useToast } from "@/components/ui/use-toast";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import axiosInstance from "@/api/axios";

type DeleteBranchDialogProps = {
  branchId: string | null;
};

export function DeleteBranchDialog({ branchId }: DeleteBranchDialogProps) {
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const deleteBranch = async () => {
    await axiosInstance.delete(`/company/${branchId}`);
  };

  const { mutateAsync: deleteBranchFn } = useMutation({
    mutationKey: ["delete-company", branchId],
    mutationFn: deleteBranch,
    onSuccess: async () => {
      queryClient.setQueryData(
        ["list-companies", "list-branches"],
        (oldData: any) => {
          return Array.isArray(oldData)
            ? oldData.filter((branch: any) => branch.id !== branchId)
            : [];
        }
      );
    },
  });

  async function handleDeleteBranch() {
    try {
      await deleteBranchFn();
      toast({
        title: "Filial deletada com sucesso!",
        description: "A lista de empresas foi atualizada",
        duration: 3000,
        className: "bg-green-700 border-none antialiasing",
      });
    } catch (error) {
      console.error(error);
      toast({
        title: "Erro!",
        description: "Não foi possível deletar a filial",
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <AlertDialogContent>
      <AlertDialogHeader>
        <AlertDialogTitle className="mb-2">
          Você tem certeza que deseja remover esta filial?
        </AlertDialogTitle>
        <AlertDialogDescription>
          Essa ação não pode ser desfeita. Isso irá excluir permanentemente de
          nossos servidores.
        </AlertDialogDescription>
      </AlertDialogHeader>
      <AlertDialogFooter className="mt-6">
        <AlertDialogCancel className="text-white bg-slate-600 hover:bg-slate-600/75">
          <Ban className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base ">Cancelar</h1>
        </AlertDialogCancel>
        <AlertDialogAction
          onClick={handleDeleteBranch}
          className="text-white bg-red-600 hover:bg-red-600/75"
        >
          <Trash2 className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base ">Remover</h1>
        </AlertDialogAction>
      </AlertDialogFooter>
    </AlertDialogContent>
  );
}
