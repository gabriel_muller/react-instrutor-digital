import {
  DialogHeader,
  DialogContent,
  DialogTitle,
  DialogDescription,
  DialogFooter,
  DialogClose,
} from "../../ui/dialog";
import { Input } from "../../ui/input";
import { Button } from "../../ui/button";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import * as React from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useToast } from "../../ui/use-toast";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../../ui/form";
import { Separator } from "../../ui/separator";
import { Ban, PlusCircle } from "lucide-react";
import { StateComboboxFilters } from "../../state-combobox-filters";
import { UF } from "@/shared/enums/state-enum";
import { cnpj } from "cpf-cnpj-validator";
// @ts-expect-error
import InputMask from "react-input-mask";
import { cleanPhoneNumber } from "@/shared/functions/format-phone-number";
import { cleanCEP } from "@/shared/functions/format-cep";
import { cleanCNPJ } from "@/shared/functions/format-cnpj";
import { getAddressByZipCode } from "@/shared/requests/get-address-by-zipcode";
import axiosInstance from "@/api/axios";
import { getCityIdByName } from "@/shared/requests";

const createBranchSchema = z.object({
  cnpj: z.string().refine(
    (value) => {
      return (
        !!cleanCNPJ(value.trim()) && cnpj.isValid(cleanCNPJ(value)) == true
      );
    },
    {
      message: "Campo CNPJ é obrigatório e deve ser um CNPJ válido",
    }
  ),

  socialReason: z
    .string()
    .max(130, "Campo deve ter no máximo 130 caracteres")
    .refine(
      (value) => {
        return !!value.trim();
      },
      {
        message: "Campo Razão Social é obrigatório",
      }
    ),

  fantasyName: z
    .string()
    .max(130, "Campo deve ter no máximo 130 caracteres")
    .refine(
      (value) => {
        return !!value.trim();
      },
      {
        message: "Campo Nome Fantasia é obrigatório",
      }
    ),
  zipcode: z.string().refine(
    (value) => {
      return !!cleanCEP(value.trim());
    },
    {
      message: "Campo Nome Fantasia é obrigatório",
    }
  ),
  address: z.string().max(100, "Campo deve ter no máximo 100 caracteres"),
  addressNumber: z.string().max(10, "Campo deve ter no máximo 10 caracteres"),
  addressComp: z
    .string()
    .max(50, "Campo deve ter no máximo 50 caracteres")
    .nullable()
    .optional(),
  addressCity: z.string().max(100, "Campo deve ter no máximo 100 caracteres"),
  addressUf: z.nativeEnum(UF, {
    errorMap: (_issue, _ctx) => {
      return { message: "Campo deve ser um estado válido" };
    },
  }),
  email: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return value === null || value === "" || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "Campo deve ser um e-mail válido ou estar vazio",
      }
    ),
  emailSecondary: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .nullable()
    .refine(
      (value) => {
        return value === null || value === "" || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "Campo deve ser um e-mail válido ou estar vazio",
      }
    )
    .optional(),

  phone: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return (
          cleanPhoneNumber(value) === null ||
          /^\d*$/.test(cleanPhoneNumber(value))
        );
      },
      {
        message: "Campo deve ter apenas números ou estar vazio",
      }
    ),
  phoneSecondary: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .nullable()
    .optional(),
});

type CreateBranchProps = {
  cnpj: string;
  socialReason: string;
  fantasyName: string;
  zipcode: string;
  address: string;
  addressNumber: string;
  addressComp: string | null;
  addressCityId: string;
  addressUf: string;
  email: string;
  emailSecondary: string | null;
  phone: string;
  phoneSecondary: string | null;
};

interface CreateBranchDialogProps {
  companyId?: string | null;
}

type CreateBranchSchema = z.infer<typeof createBranchSchema>;

export function CreateBranchDialog({ companyId }: CreateBranchDialogProps) {
  const [_selectedAddressState, setSelectedAddressState] = React.useState();

  const queryClient = useQueryClient();
  const { toast } = useToast();

  const form = useForm<CreateBranchSchema>({
    resolver: zodResolver(createBranchSchema),
  });

  const handleZipCodeChange = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const zipcode = event.target.value.replace(/\D/g, "");
    if (zipcode.length === 8) {
      const address = await getAddressByZipCode(zipcode);
      if (address) {
        form.setValue("address", address.logradouro || "");
        form.setValue("addressCity", address.localidade || "");
        form.setValue("addressUf", address.uf || "");
        form.setValue("addressNumber", "");
        form.setValue("addressComp", "");
      }
    }
  };

  const createBranch = async (data: CreateBranchProps) => {
    const response = await axiosInstance.post<CreateBranchProps>(
      `/company/${companyId}/branch`,
      data
    );
    return response.data;
  };

  const { mutateAsync: createBranchFn } = useMutation({
    mutationFn: createBranch,
    onSuccess(_, variables) {
      queryClient.setQueryData(["find-company-by-id"], (data: any) => {
        return Array.isArray(data)
          ? [
              ...data,
              {
                cnpj: variables.cnpj,
                socialReason: variables.socialReason,
                fantasyName: variables.fantasyName,
                zipcode: variables.zipcode,
                address: variables.address,
                addressNumber: variables.addressNumber,
                addressComp: variables.addressComp,
                addressCity: variables.addressCityId,
                addressUf: variables.addressUf,
                email: variables.email,
                emailSecondary: variables.emailSecondary,
                phone: variables.phone,
                phoneSecondary: variables.phoneSecondary,
              },
            ]
          : [
              {
                cnpj: variables.cnpj,
                socialReason: variables.socialReason,
                fantasyName: variables.fantasyName,
                zipcode: variables.zipcode,
                address: variables.address,
                addressNumber: variables.addressNumber,
                addressComp: variables.addressComp,
                addressCity: variables.addressCityId,
                addressUf: variables.addressUf,
                email: variables.email,
                emailSecondary: variables.emailSecondary,
                phone: variables.phone,
                phoneSecondary: variables.phoneSecondary,
              },
            ];
      });
    },
  });

  async function handleCreateBranch(data: CreateBranchSchema) {
    try {
      const addressCityId = await getCityIdByName(
        data.addressCity ? data.addressCity : ""
      );
      await createBranchFn({
        cnpj: cleanCNPJ(data.cnpj),
        socialReason: data.socialReason,
        fantasyName: data.fantasyName,
        zipcode: data.zipcode ? cleanCEP(data.zipcode) : "",
        address: data.address,
        addressNumber: data.addressNumber,
        addressComp: data.addressComp ? data.addressComp : null,
        addressCityId: addressCityId,
        addressUf: data.addressUf,
        email: data.email,
        emailSecondary: data.emailSecondary ? data.emailSecondary : null,
        phone: cleanPhoneNumber(data.phone),
        phoneSecondary: data.phoneSecondary
          ? cleanPhoneNumber(data.phoneSecondary)
          : null,
      });
      toast({
        title: "Filial criada com sucesso!",
        description: `A filial ${data.socialReason} foi criada`,
        duration: 2000,
      });
    } catch (error) {
      console.error(error);
      toast({
        title: "Erro!",
        description: `Não foi possível criar a filial ${data.socialReason}`,
        duration: 2000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <DialogContent className="w-screen max-w-6xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Cadastrar nova filial
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleCreateBranch)}
          className="space-y-4"
        >
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="cnpj"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>
                    CNPJ<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <InputMask
                      mask="99.999.999/9999-99"
                      placeholder="Digite o CNPJ"
                      {...field}
                      {...form.register("cnpj")}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage className="mt-4" />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="socialReason"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>
                    Razão Social<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a razão social"
                      {...field}
                      {...form.register("socialReason")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="fantasyName"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>
                    Nome Fantasia<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome fantasia"
                      {...field}
                      {...form.register("fantasyName")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>

          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Logradouro
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="zipcode"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>CEP</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="99999-999"
                      placeholder="Digite o CEP"
                      {...field}
                      {...form.register("zipcode")}
                      onChange={handleZipCodeChange}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="address"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>Logradouro</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o endereço"
                      {...field}
                      {...form.register("address")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressNumber"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>Número</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a número"
                      {...field}
                      {...form.register("addressNumber")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressComp"
              render={() => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Complemento</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o complemento"
                      {...form.register("addressComp")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressCity"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Cidade</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a cidade"
                      {...field}
                      {...form.register("addressCity")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressUf"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Estado</FormLabel>
                  <FormControl>
                    <StateComboboxFilters
                      onSelect={(selectedValue: any) => {
                        setSelectedAddressState(selectedValue);
                        form.setValue("addressUf", selectedValue);
                      }}
                      {...field}
                      {...form.register("addressUf")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Contato
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>E-mail Principal</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o e-mail"
                      {...field}
                      {...form.register("email")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="emailSecondary"
              render={() => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>E-mail Secundário</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o e-mail secundário"
                      {...form.register("emailSecondary")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="phone"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Telefone Principal</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone"
                      {...field}
                      {...form.register("phone")}
                      value={field.value}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="phoneSecondary"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Telefone Secundário</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone secundário"
                      {...field}
                      {...form.register("phoneSecondary")}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <Separator className="col-span-12 mt-6" />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button
                type="button"
                className="text-white antialiased bg-slate-600 hover:bg-slate-600/75"
              >
                <Ban className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base">Cancelar</h1>
              </Button>
            </DialogClose>
            <Button
              type="submit"
              className="text-white antialiased bg-green-700 hover:bg-green-700/75"
            >
              <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base">Adicionar</h1>
            </Button>
          </DialogFooter>
        </form>
      </Form>
    </DialogContent>
  );
}
