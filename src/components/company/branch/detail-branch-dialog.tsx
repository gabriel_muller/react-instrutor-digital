import { useQuery } from "@tanstack/react-query";
import axiosInstance from "@/api/axios";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import {
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  Form,
  Input,
  Label,
  Separator,
} from "@/components/ui";
import { UF } from "@/shared/enums";
import { formatCEP, formatCNPJ, formatPhoneNumber } from "@/shared/functions";

const detailBranchSchema = z.object({
  cnpj: z.string().optional(),
  socialReason: z.string().optional(),
  fantasyName: z.string().optional(),
  zipcode: z.string().optional(),
  address: z.string().optional(),
  addressNumber: z.string().optional(),
  addressComp: z.string().nullable().optional(),
  addressCity: z.string().optional(),
  addressUf: z.string().optional(),
  email: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return value === null || value === "" || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "Campo deve ser um e-mail válido ou estar vazio",
      }
    )
    .optional(),
  emailSecondary: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .nullable()
    .refine(
      (value) => {
        return value === null || value === "" || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "Campo deve ser um e-mail válido ou estar vazio",
      }
    )
    .optional(),
  phone: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return value === null || /^\d*$/.test(value);
      },
      {
        message: "Campo deve ter apenas números ou estar vazio",
      }
    )
    .optional(),
  phoneSecondary: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .nullable()
    .refine(
      (value) => {
        return value === null || /^\d*$/.test(value);
      },
      {
        message: "Campo deve ter apenas números ou estar vazio",
      }
    )
    .optional(),
  headOffice: z.boolean().optional(),
});

type FindBranchByIdProps = {
  cnpj: string;
  socialReason: string;
  fantasyName: string;
  address: string;
  addressNumber: string;
  addressComp: string;
  zipcode: string;
  city: {
    name: string;
    state: UF;
  };
  email: string;
  emailSecondary: string;
  phone: string;
  phoneSecondary: string;
};

type DeleteBranchDialogProps = {
  branchId: string;
};

type DetailBranchSchema = z.infer<typeof detailBranchSchema>;

export function DetailBranchDialog({ branchId }: DeleteBranchDialogProps) {
  const findBranchById = async (id: string) => {
    const response = await new Promise((resolve) => {
      setTimeout(async () => {
        const res = await axiosInstance.get<FindBranchByIdProps>(
          `/company/${id}`
        );
        resolve(res);
      }, 1000);
    });
    return (response as any).data;
  };

  const { data: branch } = useQuery({
    queryKey: ["find-branch-by-id", branchId],
    queryFn: () => findBranchById(branchId as string),
  });

  const form = useForm<DetailBranchSchema>({
    resolver: zodResolver(detailBranchSchema),
  });

  return (
    <DialogContent className="w-screen max-w-6xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Detalhes filial
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form className="space-y-4">
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <div className="col-span-4 flex flex-col">
              <Label htmlFor="cnpj" className="my-2  text-base">
                CNPJ
              </Label>

              <Input
                id="cnpj"
                value={formatCNPJ(branch?.cnpj ? branch.cnpj : "")}
                disabled
              />
            </div>

            <div className="col-span-4 flex flex-col">
              <Label htmlFor="socialReason" className="my-2  text-base">
                Razão Social
              </Label>

              <Input
                id="socialReason"
                value={branch?.socialReason ? branch.socialReason : ""}
                disabled
              />
            </div>
            <div className="col-span-4 flex flex-col">
              <Label htmlFor="fantastName" className="my-2  text-base">
                Nome Fantasia
              </Label>

              <Input
                id="fantastName"
                value={branch?.fantasyName ? branch.fantasyName : ""}
                disabled
              />
            </div>
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Logradouro
            </DialogDescription>
            <Separator className="col-span-12" />
            <div className="col-span-2 flex flex-col">
              <Label htmlFor="zipcode" className="my-2  text-base">
                CEP
              </Label>

              <Input
                id="zipcode"
                value={formatCEP(branch?.zipcode ? branch.zipcode : "")}
                disabled
              />
            </div>
            <div className="col-span-5 flex flex-col">
              <Label htmlFor="address" className="my-2  text-base">
                Endereço
              </Label>

              <Input
                id="address"
                value={formatCEP(branch?.address ? branch.address : "")}
                disabled
              />
            </div>
            <div className="col-span-2 flex flex-col">
              <Label htmlFor="addressNumber" className="my-2  text-base">
                Número
              </Label>

              <Input
                id="addressNumber"
                value={branch?.addressNumber ? branch.addressNumber : ""}
                disabled
              />
            </div>
            <div className="col-span-3 flex flex-col">
              <Label htmlFor="addressComp" className="my-2  text-base">
                Complemento
              </Label>

              <Input
                id="addressComp"
                value={branch?.addressComp ? branch.addressComp : ""}
                disabled
              />
            </div>
            <div className="col-span-2 flex flex-col">
              <Label htmlFor="addressUf" className="my-2  text-base">
                Estado
              </Label>

              <Input
                id="addressUf"
                value={branch?.city?.state ? branch.city.state : null}
                disabled
              />
            </div>
            <div className="col-span-2 flex flex-col">
              <Label htmlFor="addressCity" className="my-2  text-base">
                Cidade
              </Label>

              <Input
                id="addressCity"
                value={branch?.city?.name ? branch.city.name : ""}
                disabled
              />
            </div>
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold mt-6">
              Contato
            </DialogDescription>
            <Separator className="col-span-12" />
            <div className="col-span-3 flex flex-col">
              <Label htmlFor="email" className="my-2  text-base">
                E-mail
              </Label>

              <Input
                id="email"
                value={branch?.email ? branch.email : ""}
                disabled
              />
            </div>
            <div className="col-span-3 flex flex-col">
              <Label htmlFor="emailSecondary" className="my-2  text-base">
                E-mail Secundário
              </Label>

              <Input
                id="emailSecondary"
                value={branch?.emailSecondary ? branch.emailSecondary : ""}
                disabled
              />
            </div>
            <div className="col-span-3 flex flex-col">
              <Label htmlFor="phone" className="my-2  text-base">
                Telefone
              </Label>

              <Input
                id="phone"
                value={branch?.phone ? formatPhoneNumber(branch.phone) : ""}
                disabled
              />
            </div>
            <div className="col-span-3 flex flex-col">
              <Label htmlFor="phoneSecondary" className="my-2  text-base">
                Telefone Secundário
              </Label>

              <Input
                id="phoneSecondary"
                value={
                  branch?.phoneSecondary
                    ? formatPhoneNumber(branch.phoneSecondary)
                    : ""
                }
                disabled
              />
            </div>
          </div>
        </form>
      </Form>
    </DialogContent>
  );
}
