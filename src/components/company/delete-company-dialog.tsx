import { Ban, Trash2 } from "lucide-react";
import {
  AlertDialogHeader,
  AlertDialogFooter,
  AlertDialogContent,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogDescription,
  AlertDialogTitle,
} from "../ui/alert-dialog";
import { useToast } from "@/components/ui/use-toast";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import axiosInstance from "@/api/axios";

type DeleteCompanyDialogProps = {
  companyId: string | null;
};

export function DeleteCompanyDialog({ companyId }: DeleteCompanyDialogProps) {
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const deleteCompany = async () => {
    await axiosInstance.delete(`/company/${companyId}`);
  };

  const { mutateAsync: deleteCompanyFn } = useMutation({
    mutationKey: ["delete-company", companyId],
    mutationFn: deleteCompany,
    onSuccess: async () => {
      queryClient.setQueryData(["list-companies"], (oldData: any) => {
        if (!oldData) return [];
        return oldData.filter((company: any) => company.id !== companyId);
      });
    },
  });

  async function handleDeleteCompany() {
    try {
      await deleteCompanyFn();
      toast({
        title: "Empresa deletada com sucesso!",
        description: "A lista de empresas foi atualizada",
        duration: 3000,
        className: "bg-green-700 border-none antialiasing",
      });
    } catch (error) {
      console.error(error);
      toast({
        title: "Erro!",
        description: "Não foi possível deletar a empresa",
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <AlertDialogContent>
      <AlertDialogHeader>
        <AlertDialogTitle className="mb-2">
          Você tem certeza que deseja remover esta empresa?
        </AlertDialogTitle>
        <AlertDialogDescription>
          Essa ação não pode ser desfeita. Isso irá excluir permanentemente sua
          conta e remover seus dados de nossos servidores.
        </AlertDialogDescription>
      </AlertDialogHeader>
      <AlertDialogFooter className="mt-6">
        <AlertDialogCancel className="text-white bg-slate-600 hover:bg-slate-600/75">
          <Ban className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base">Cancelar</h1>
        </AlertDialogCancel>
        <AlertDialogAction
          onClick={handleDeleteCompany}
          className="text-white bg-red-600 hover:bg-red-600/75"
        >
          <Trash2 className="w-4 h-4 mr-2 mb-[2px]" />
          <h1 className="text-base">Remover</h1>
        </AlertDialogAction>
      </AlertDialogFooter>
    </AlertDialogContent>
  );
}
