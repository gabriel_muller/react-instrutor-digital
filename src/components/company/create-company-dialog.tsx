import {
  DialogHeader,
  DialogContent,
  DialogTitle,
  DialogDescription,
  DialogFooter,
  DialogClose,
} from "../ui/dialog";
import { Input } from "../ui/input";
import { Button } from "../ui/button";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import * as React from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useToast } from "../ui/use-toast";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
import { Separator } from "../ui/separator";
import { Ban, PlusCircle } from "lucide-react";
import { StateComboboxFilters } from "../state-combobox-filters";
import { UF } from "@/shared/enums/state-enum";
import { cnpj } from "cpf-cnpj-validator";
// @ts-expect-error
import InputMask from "react-input-mask";
import { cleanPhoneNumber } from "@/shared/functions/format-phone-number";
import { cleanCNPJ } from "@/shared/functions/format-cnpj";
import { cleanCEP } from "@/shared/functions/format-cep";
import { CustomMaskedInput } from "../ui/custom-masked-input";
import { CustomInput } from "../ui/custom-input";
import { LoadingButton } from "../ui/loading-button";
import { getCityIdByName } from "@/shared/requests/get-city-id-by-name";
import { getAddressByZipCode } from "@/shared/requests/get-address-by-zipcode";
import axiosInstance from "@/api/axios";

const createCompanySchema = z.object({
  cnpj: z.string().refine(
    (value) => {
      return !!value.trim() && cnpj.isValid(value) == true;
    },
    {
      message: "Campo CNPJ é obrigatório e deve ser um CNPJ válido",
    }
  ),

  socialReason: z
    .string()
    .max(130, "Campo deve ter no máximo 130 caracteres")
    .refine(
      (value) => {
        return !!value.trim();
      },
      {
        message: "Campo Razão Social é obrigatório",
      }
    ),

  fantasyName: z
    .string()
    .max(130, "Campo deve ter no máximo 130 caracteres")
    .refine(
      (value) => {
        return !!value.trim();
      },
      {
        message: "Campo Nome Fantasia é obrigatório",
      }
    ),
  zipcode: z
    .string()
    .max(9, "Campo deve ter no máximo 9 caracteres")
    .optional(),
  address: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .optional(),
  addressNumber: z
    .string()
    .max(10, "Campo deve ter no máximo 10 caracteres")
    .optional(),
  addressComp: z
    .string()
    .max(50, "Campo deve ter no máximo 50 caracteres")
    .optional(),
  addressCity: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .optional(),
  addressUf: z
    .nativeEnum(UF, {
      errorMap: (_issue, _ctx) => {
        return { message: "Campo deve ser um estado válido" };
      },
    })
    .optional(),
  email: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return value === null || value === "" || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "Campo deve ser um e-mail válido ou estar vazio",
      }
    )
    .optional(),
  emailSecondary: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return value === null || value === "" || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "Campo deve ser um e-mail válido ou estar vazio",
      }
    )
    .optional(),

  phone: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return (
          cleanPhoneNumber(value) === null ||
          /^\d*$/.test(cleanPhoneNumber(value))
        );
      },
      {
        message: "Campo deve ter apenas números ou estar vazio",
      }
    )
    .optional(),
  phoneSecondary: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return (
          cleanPhoneNumber(value) === null ||
          /^\d*$/.test(cleanPhoneNumber(value))
        );
      },
      {
        message: "Campo deve ter apenas números ou estar vazio",
      }
    )
    .optional(),
});

type CreateCompanyProps = {
  cnpj: string;
  socialReason: string;
  fantasyName: string;
  zipcode?: string;
  address?: string;
  addressNumber?: string;
  addressComp?: string | null;
  addressCity?: string;
  addressCityId?: number;
  addressUf?: string;
  email?: string;
  emailSecondary?: string | null;
  phone?: string;
  phoneSecondary?: string | null;
};

type CreateCompanySchema = z.infer<typeof createCompanySchema>;

export function CreateCompanyDialog() {
  const [_selectedAddressState, setSelectedAddressState] = React.useState();
  const [isLoading, setIsLoading] = React.useState(false);

  const queryClient = useQueryClient();
  const { toast } = useToast();

  const form = useForm<CreateCompanySchema>({
    resolver: zodResolver(createCompanySchema),
  });

  const handleZipCodeChange = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const zipcode = event.target.value.replace(/\D/g, "");
    if (zipcode.length === 8) {
      const address = await getAddressByZipCode(zipcode);
      if (address) {
        form.setValue("address", address.logradouro || "");
        form.setValue("addressCity", address.localidade || "");
        form.setValue("addressUf", address.uf || "");
        form.setValue("addressNumber", "");
        form.setValue("addressComp", "");
      }
    }
  };

  const createCompany = async (data: CreateCompanyProps) => {
    try {
      const response = await axiosInstance.post<CreateCompanyProps>(
        "/company",
        data
      );
      return response.data;
    } catch (error) {
      throw new Error("Erro ao criar empresa");
    }
  };

  const { mutateAsync: createCompanyFn } = useMutation({
    mutationFn: createCompany,
    onSuccess(_data, variables) {
      queryClient.setQueryData(["list-companies"], (existingCompanies: any) => [
        ...(existingCompanies || []),
        {
          cnpj: variables.cnpj,
          socialReason: variables.socialReason,
          fantasyName: variables.fantasyName,
          zipcode: variables.zipcode,
          address: variables.address,
          addressNumber: variables.addressNumber,
          addressComp: variables.addressComp,
          addressCity: variables.addressCity,
          addressUf: variables.addressUf,
          email: variables.email,
          emailSecondary: variables.emailSecondary,
          phone: variables.phone,
          phoneSecondary: variables.phoneSecondary,
        },
      ]);
      setIsLoading(false);
      toast({
        title: "Empresa criada com sucesso!",
        description: `A empresa ${variables.socialReason} foi criada`,
        duration: 3000,
        className: "bg-green-700 border-none antialiasing",
      });
    },
    onError(_error) {
      setIsLoading(false);
      toast({
        title: "Erro!",
        description: `Não foi possível criar a empresa.`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    },
  });

  async function handleCreateCompany(data: CreateCompanySchema) {
    const addressCityId = await getCityIdByName(
      data.addressCity ? data.addressCity : ""
    );
    setIsLoading(true);
    try {
      await createCompanyFn({
        cnpj: cleanCNPJ(data.cnpj),
        socialReason: data.socialReason,
        fantasyName: data.fantasyName,
        zipcode: cleanCEP(data.zipcode ?? ""),
        address: data.address,
        addressNumber: data.addressNumber,
        addressComp: data.addressComp ? data.addressComp : null,
        addressCityId: addressCityId,
        addressUf: data.addressUf,
        email: data.email,
        emailSecondary: data.emailSecondary ? data.emailSecondary : null,
        phone: cleanPhoneNumber(data.phone ?? ""),
        phoneSecondary: data.phoneSecondary
          ? cleanPhoneNumber(data.phoneSecondary)
          : null,
      });
    } catch (error) {
      setIsLoading(false);
      toast({
        title: "Erro!",
        description: `Não foi possível criar a empresa ${data.socialReason}`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <DialogContent className="w-screen max-w-6xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Cadastrar nova empresa
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleCreateCompany)}
          className="space-y-4"
        >
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <CustomMaskedInput
              control={form.control}
              name="cnpj"
              label="CNPJ"
              mask="99.999.999/9999-99"
              placeholder="Digite o CNPJ"
              register={form.register}
              required={true}
              className="col-span-4"
            />

            <CustomInput
              control={form.control}
              name="socialReason"
              label="Razão Social"
              placeholder="Digite a razão social"
              register={form.register}
              required={true}
              className="col-span-4"
            />

            <CustomInput
              control={form.control}
              name="fantasyName"
              label="Nome Fantasia"
              placeholder="Digite o nome fantasia"
              register={form.register}
              required={true}
              className="col-span-4"
            />
          </div>

          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Localização
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="zipcode"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>
                    CEP<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <InputMask
                      mask="99999-999"
                      placeholder="Digite o CEP"
                      {...field}
                      {...form.register("zipcode")}
                      onChange={handleZipCodeChange}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <CustomInput
              control={form.control}
              name="address"
              label="Logradouro"
              placeholder="Digite o logradouro"
              register={form.register}
              required={true}
              className="col-span-5"
            />
            <CustomInput
              control={form.control}
              name="addressNumber"
              label="Número"
              placeholder="Digite o número"
              register={form.register}
              required={true}
              className="col-span-2"
            />
            <CustomInput
              control={form.control}
              name="addressComp"
              label="Complemento"
              placeholder="Digite o complemento"
              register={form.register}
              required={false}
              className="col-span-3"
            />

            <FormField
              control={form.control}
              name="addressUf"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>
                    Estado<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <StateComboboxFilters
                      onSelect={(selectedValue: any) => {
                        setSelectedAddressState(selectedValue);
                        form.setValue("addressUf", selectedValue);
                      }}
                      {...field}
                      {...form.register("addressUf")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <CustomInput
              control={form.control}
              name="addressCity"
              label="Cidade"
              placeholder="Digite a cidade"
              register={form.register}
              required={true}
              className="col-span-3"
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Contato
            </DialogDescription>
            <Separator className="col-span-12" />
            <CustomInput
              control={form.control}
              name="email"
              label="E-mail Principal"
              placeholder="Digite o e-mail principal"
              register={form.register}
              required={true}
              className="col-span-3"
            />
            <CustomInput
              control={form.control}
              name="emailSecondary"
              label="E-mail Secundário"
              placeholder="Digite o e-mail secundário"
              register={form.register}
              required={false}
              className="col-span-3"
            />
            <CustomMaskedInput
              control={form.control}
              name="phone"
              label="Telefone Principal"
              mask="(99) 99999-9999"
              placeholder="Digite o telefone principal"
              register={form.register}
              required={true}
              className="col-span-3"
            />
            <CustomMaskedInput
              control={form.control}
              name="phoneSecondary"
              label="Telefone Secundário"
              mask="(99) 99999-9999"
              placeholder="Digite o telefone secundário"
              register={form.register}
              required={false}
              className="col-span-3"
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <Separator className="col-span-12 mt-6" />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button
                type="button"
                className="text-white antialiased bg-slate-600 hover:bg-slate-600/75"
              >
                <Ban className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base">Cancelar</h1>
              </Button>
            </DialogClose>
            <LoadingButton
              loading={isLoading}
              type="submit"
              className="antialiased bg-green-700 hover:bg-green-700/75 text-white"
            >
              <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base">Adicionar</h1>
            </LoadingButton>
          </DialogFooter>
        </form>
      </Form>
    </DialogContent>
  );
}
