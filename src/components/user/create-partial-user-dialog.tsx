import {
  DialogHeader,
  DialogContent,
  DialogTitle,
  DialogFooter,
  DialogClose,
} from "../ui/dialog";
import { Input } from "../ui/input";
import { Button } from "../ui/button";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useToast } from "../ui/use-toast";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
// @ts-expect-error
import InputMask from "react-input-mask";
import { Separator } from "../ui/separator";
import { Ban, PlusCircle } from "lucide-react";
import { cleanPhoneNumber } from "@/shared/functions/format-phone-number";
import { GlobalAccessLevel } from "@/shared/enums/access-level-enum";
import { AccessLevelComboboxFilters } from "../access-level-combobox-filters";
import React from "react";
import { formatAccessLevel } from "@/shared/functions/format-access-level";
import { LoadingButton } from "../ui/loading-button";
import axiosInstance from "@/api/axios";

const createPartialUserSchema = z.object({
  name: z.string().refine(
    (value) => {
      return !!value.trim() || /^\S+@\S+\.\S+$/.test(value);
    },
    {
      message: "Nome é obrigatório",
    }
  ),
  email: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        return !!value.trim() || /^\S+@\S+\.\S+$/.test(value);
      },
      {
        message: "E-mail inválido",
      }
    ),
  phone: z
    .string()
    .max(100, "Campo deve ter no máximo 100 caracteres")
    .refine(
      (value) => {
        console.log(value.length);
        return !!value.trim();
      },
      {
        message: "Telefone é obrigatorio",
      }
    ),
  accessLevel: z.nativeEnum(GlobalAccessLevel, {
    errorMap: (_issue, _ctx) => {
      return { message: "Campo deve ser um nível de acesso válido" };
    },
  }),
});

type CreatePartialUserProps = {
  name: string;
  email: string;
  phone: string;
  accessLevel: any;
};

type CreatePartialUserSchema = z.infer<typeof createPartialUserSchema>;

export function CreatePartialUserDialog() {
  const [_selectedAccessLevel, setSelectedAccessLevel] = React.useState();
  const [isLoading, setIsLoading] = React.useState(false);
  const queryClient = useQueryClient();
  const { toast } = useToast();

  const form = useForm<CreatePartialUserSchema>({
    resolver: zodResolver(createPartialUserSchema),
  });

  const createPartialUser = async (data: CreatePartialUserProps) => {
    const response = await axiosInstance.post<CreatePartialUserProps>(
      "/user/register",
      data
    );
    return response.data;
  };

  const { mutateAsync: createPartialUserFn } = useMutation({
    mutationFn: createPartialUser,
    onSuccess(_, variables) {
      queryClient.setQueryData(["list-pre-register-user"], (data: any) => {
        const existingData = Array.isArray(data) ? data : [];
        return [
          ...existingData,
          {
            name: variables.name,
            email: variables.email,
            phone: variables.phone,
            accessLevel: variables.accessLevel,
          },
        ];
      });
      setIsLoading(false);
    },
    onError(_error) {
      setIsLoading(false);
      toast({
        title: "Erro no pré cadastro!",
        description: `Não foi possível criar o pré cadastro`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    },
  });

  async function handleCreatePartialUser(data: CreatePartialUserSchema) {
    setIsLoading(true);
    try {
      await createPartialUserFn({
        name: data.name,
        email: data.email,
        phone: cleanPhoneNumber(data.phone),
        accessLevel: formatAccessLevel(data.accessLevel),
      });
      toast({
        title: "Pré cadastro concluído com sucesso!",
        description: `Verifique o e-mail ${data.email} para finalizar o cadastro.`,
        duration: 3000,
      });
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      toast({
        title: "Erro no pré cadastro!",
        description: `Não foi possível criar o pré cadastro para o e-mail ${data.email}`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <DialogContent className="w-screen max-w-2xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Pré cadastro de novo usuário
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleCreatePartialUser)}
          className="space-y-4"
        >
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Nome completo<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome completo"
                      {...field}
                      {...form.register("name")}
                    ></Input>
                  </FormControl>
                  <FormMessage className="mt-4" />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    E-mail<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o e-mail"
                      {...field}
                      {...form.register("email")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>

          <div className="grid grid-cols-12 items-center text-left gap-6">
            <FormField
              control={form.control}
              name="phone"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Telefone<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone"
                      {...field}
                      {...form.register("phone")}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="accessLevel"
              render={({ field }) => (
                <FormItem className="col-span-6 flex flex-col">
                  <FormLabel>
                    Nível de Acesso<b className="text-red-600 mx-1"> *</b>
                  </FormLabel>
                  <FormControl>
                    <AccessLevelComboboxFilters
                      onSelect={(selectedValue: any) => {
                        setSelectedAccessLevel(selectedValue);
                        form.setValue("accessLevel", selectedValue);
                      }}
                      {...field}
                      {...form.register("accessLevel")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <Separator className="col-span-12 mt-6" />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button
                type="button"
                className="text-white antialiased bg-slate-600 hover:bg-slate-600/75"
              >
                <Ban className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base ">Cancelar</h1>
              </Button>
            </DialogClose>
            <LoadingButton
              loading={isLoading}
              type="submit"
              className="antialiased bg-green-700 hover:bg-green-700/75 text-white"
            >
              <PlusCircle className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base">Adicionar</h1>
            </LoadingButton>
          </DialogFooter>
        </form>
      </Form>
    </DialogContent>
  );
}
