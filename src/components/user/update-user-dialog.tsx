import { Ban, Pencil } from "lucide-react";
import {
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "../ui/dialog";
import { Input } from "../ui/input";
import { string, z } from "zod";
import React from "react";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useToast } from "../ui/use-toast";
import { Separator } from "../ui/separator";
import { Button } from "../ui/button";
import { StateComboboxFilters } from "../state-combobox-filters";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
// @ts-expect-error
import InputMask from "react-input-mask";
import { UF } from "@/shared/enums/state-enum";
import axiosInstance from "@/api/axios";

const updateUserSchema = z.object({
  name: z.string().optional(),
  username: z.string().optional(),
  function: z.string().optional(),
  registry: z.string().optional(),
  birthdate: z.string().optional(),
  avatar: z.string().optional(),
  userGroupId: z.number().optional(),
  address: string().optional(),
  addressNumber: z.string().optional(),
  addressComp: z.string().optional(),
  addressCity: z.string().optional(),
  addressUf: z.string().optional(),
  phone: z.string().optional(),
  phoneSecondary: z.string().optional(),
});

type UpdateUserProps = {
  name?: string;
  username?: string;
  function?: string;
  registry?: string;
  birthdate?: string;
  avatar?: string;
  userGroupId?: number;
  address?: string;
  addressNumber?: string;
  addressComp?: string;
  addressCity?: string;
  addressUf?: string;
  phone?: string;
  phoneSecondary?: string;
};

type FindUserByIdProps = {
  name: string;
  username: string;
  function: string;
  registry: string;
  birthdate: string;
  avatar: string;
  userGroupId: number;
  address: string;
  addressNumber: string;
  addressComp: string;
  addressCity: string;
  addressUf: string;
  phone: string;
  phoneSecondary: string;
  city: {
    name: string;
    state: UF;
  };
};

type UpdateUserDialogProps = {
  userId: string;
};

type UpdateUserSchema = z.infer<typeof updateUserSchema>;

export function UpdateUserDialog({ userId }: UpdateUserDialogProps) {
  const [_selectedAddressState, setSelectedAddressState] = React.useState("");

  const queryClient = useQueryClient();
  const { toast } = useToast();

  const updateUser = async (data: UpdateUserProps) => {
    const response = await axiosInstance.patch<UpdateUserProps>(
      `/user/${userId}`,
      data
    );
    return response.data;
  };

  const findUserById = async (id: string) => {
    if (id) {
      const response = await axiosInstance.get<FindUserByIdProps>(
        `/user/${id}`
      );
      console.log(response.data);
      form.setValue("name", response.data.name);
      form.setValue("username", response.data.username);
      form.setValue("function", response.data.function);
      form.setValue("registry", response.data.registry);
      form.setValue("birthdate", response.data.birthdate);
      form.setValue("userGroupId", response.data.userGroupId);
      form.setValue("avatar", response.data.avatar);
      form.setValue("address", response.data.address);
      form.setValue("addressNumber", response.data.addressNumber);
      form.setValue("addressComp", response.data.addressComp);
      form.setValue("addressCity", response.data.city.name);
      form.setValue("addressUf", response.data.city.state);
      form.setValue("phone", response.data.phone);
      form.setValue("phoneSecondary", response.data.phoneSecondary ?? "");
      return response.data;
    }
  };

  const { mutateAsync: updateUserFn } = useMutation({
    mutationKey: ["update-user", userId],
    mutationFn: updateUser,
    onSuccess(_, variables) {
      queryClient.setQueryData(["list-users"], (data: any) => {
        const updatedData = data.map((user: any) => {
          if (user.id === userId) {
            return {
              ...user,
              name: variables.name,
              username: variables.username,
              function: variables.function,
              registry: variables.registry,
              birthdate: variables.birthdate,
              userGroupId: variables.userGroupId,
              avatar: variables.avatar,
              address: variables.address,
              addressNumber: variables.addressNumber,
              addressComp: variables.addressComp,
              addressCity: variables.addressCity,
              addressUf: variables.addressUf,
              phone: variables.phone,
              phoneSecondary: variables.phoneSecondary,
            };
          }
          return user;
        });
        return updatedData;
      });
    },
  });

  useQuery({
    queryKey: ["find-user-by-id", userId],
    queryFn: () => findUserById(userId),
  });

  const form = useForm<UpdateUserSchema>({
    resolver: zodResolver(updateUserSchema),
  });

  async function handleUpdateUser() {
    try {
      await updateUserFn(form.getValues());
      toast({
        title: "Usuário editado com sucesso!",
        description: `A lista de usuários foi atualizada`,
        duration: 3000,
      });
    } catch (error) {
      toast({
        title: "Erro!",
        description: `Não foi possível editar o usuário`,
        duration: 3000,
        variant: "destructive",
        className: "bg-red-700 border-none antialiasing",
      });
    }
  }

  return (
    <DialogContent className="w-screen max-w-6xl">
      <DialogHeader>
        <DialogTitle className="text-white antialiased text-xl">
          Editar usuário
        </DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(handleUpdateUser)}
          className="space-y-4"
        >
          <div className="grid grid-cols-12 items-center text-left gap-6 mb-10">
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem className="col-span-4 flex flex-col">
                  <FormLabel>Nome completo</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome completo"
                      {...field}
                      {...form.register("name")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="username"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Nome de Usuário</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o nome de usuário"
                      {...field}
                      {...form.register("username")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="function"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Função</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a função"
                      {...field}
                      {...form.register("function")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="registry"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>Registro</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o registro"
                      {...field}
                      {...form.register("registry")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="birthdate"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>Data de nascimento</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Selecione a data de aniversário"
                      {...field}
                      {...form.register("birthdate")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="avatar"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>Avatar</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Selecione a data de aniversário"
                      {...field}
                      {...form.register("avatar")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="userGroupId"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>ID do grupo de usuário</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Selecione a data de aniversário"
                      {...field}
                      {...form.register("userGroupId")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Logradouro
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="address"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Endereço</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o CEP"
                      {...field}
                      {...form.register("address")}
                    ></Input>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressNumber"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>Número</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a número"
                      {...field}
                      {...form.register("addressNumber")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressComp"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Complemento</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite o complemento"
                      {...field}
                      {...form.register("addressComp")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressCity"
              render={({ field }) => (
                <FormItem className="col-span-2 flex flex-col">
                  <FormLabel>Cidade</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a cidade"
                      {...field}
                      {...form.register("addressCity")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="addressUf"
              render={({ field }) => (
                <FormItem className="col-span-1 flex flex-col">
                  <FormLabel>Estado</FormLabel>
                  <FormControl>
                    <StateComboboxFilters
                      onSelect={(selectedValue) => {
                        setSelectedAddressState(selectedValue);
                        form.setValue("addressUf", selectedValue);
                      }}
                      {...field}
                      {...form.register("addressUf")}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <DialogDescription className="col-span-12 flex self-baseline text-lg font-semibold">
              Contato
            </DialogDescription>
            <Separator className="col-span-12" />
            <FormField
              control={form.control}
              name="phone"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Telefone Principal</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone"
                      {...field}
                      {...form.register("phone")}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="phoneSecondary"
              render={({ field }) => (
                <FormItem className="col-span-3 flex flex-col">
                  <FormLabel>Telefone Secundário</FormLabel>
                  <FormControl>
                    <InputMask
                      mask="(99) 99999-9999"
                      placeholder="Digite o telefone secundário"
                      {...field}
                      {...form.register("phoneSecondary")}
                    >
                      {(inputProps: any) => <Input {...inputProps} />}
                    </InputMask>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className="grid grid-cols-12 items-center text-left gap-6">
            <Separator className="col-span-12 mt-6" />
          </div>
          <DialogFooter>
            <DialogClose asChild>
              <Button
                type="button"
                className="text-white bg-slate-600 hover:bg-slate-600/75"
              >
                <Ban className="w-4 h-4 mr-2 mb-[2px]" />
                <h1 className="text-base ">Cancelar</h1>
              </Button>
            </DialogClose>
            <Button
              type="submit"
              className="text-white bg-amber-600 hover:bg-amber-600/75"
            >
              <Pencil className="w-4 h-4 mr-2 mb-[2px]" />
              <h1 className="text-base ">Atualizar</h1>
            </Button>
          </DialogFooter>
        </form>
      </Form>
    </DialogContent>
  );
}
