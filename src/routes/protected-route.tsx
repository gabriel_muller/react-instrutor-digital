import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { usePermissions } from "../contexts/auth-context";

interface ProtectedRouteProps {
  children: React.ReactNode;
  requiredRoles?: string[];
}

export const ProtectedRoute: React.FC<ProtectedRouteProps> = ({
  children,
  requiredRoles = [],
}) => {
  const navigate = useNavigate();
  const { roles, loading } = usePermissions();
  const allowedPaths = ["/login", "/usuario/finalizar-cadastro"];

  useEffect(() => {
    const token = localStorage.getItem("access_token");
    const currentPath = window.location.pathname;

    if (!token && !allowedPaths.includes(currentPath)) {
      navigate("/login");
      return;
    }

    if (token && !loading && requiredRoles.length > 0) {
      const hasRequiredRoles = requiredRoles.some((role) =>
        roles.includes(role)
      );
      if (!hasRequiredRoles) {
        navigate("/acesso-negado");
      }
    }
  }, [navigate, roles, loading, requiredRoles]);

  return <>{children}</>;
};
